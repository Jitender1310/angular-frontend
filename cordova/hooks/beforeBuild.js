var fs = require('fs');
fs.readFile('www/main.bundle.js', 'utf8', function (err, data) {
    if (err) {
        return console.log(err);
    }
    var result = data.replace(/assets\/img\//g, 'android_asset/www/img/');

    fs.writeFile('www/main.bundle.js', result, 'utf8', function (err) {
        if (err) return console.log(err);
        console.log("Replace of image path Successful.");
    });
});