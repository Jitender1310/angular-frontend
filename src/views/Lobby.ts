import { Container, Texture, Sprite, Graphics, TextStyle, Text } from "pixi.js";
import { Games } from "entity/Games";
import * as global from "app";

export class Lobby {

    private bg;
    private deal;
    private points;
    private pool;
    private cash;
    private tournament;
    private strcurrencyType: String;
    private practice;
    private user;
    private addCash;
    private funCoins;
    private bonus;
    private dealspr;
    private home;
    private container;
    private DataGrid;
    private mouseYPos;
    private current_token:string;
    private current_type:string;
    private strBasePath:string; 

    constructor()
    {
        this.strBasePath = global.app.getBasePath();
    }

    public loadView() {
        this.container = new Container();
        const startContainer = new Container();
        startContainer.name = "Start";
        global.app.getStage().addChild(this.container);
        this.bg = Texture.from(this.strBasePath + "background.png");//lobbyScreen.jpg");//background.png");
        const bunny = new Sprite(this.bg);
        // bunny.anchor.set(0.5);
        bunny.x = 0;
        bunny.y = 0;
        bunny.width = window.innerWidth;
        bunny.height = window.innerHeight;
        bunny.anchor.set(0);
        this.deal = Texture.from(this.strBasePath + "deal-game-select-notext.png");
        this.deal.scale = 0.7;
        this.points = Texture.from(this.strBasePath + "point-game-select-notext.png");
        this.points.scale = 0.7;
        this.pool = Texture.from(this.strBasePath + "pool-game-select-notext.png");
        this.pool.scale = 0.7;
        this.dealspr = new Sprite(this.deal);
        this.dealspr.x = 3*(window.innerWidth)/4;
        this.dealspr.y = ((window.innerHeight)/3) -30;// + 100;
        this.dealspr.scale.x = 0.7;
        this.dealspr.scale.y = 0.7;
        this.dealspr.interactive = true;
        this.dealspr.name = "deal";
        this.dealspr.buttonMode = true;
        this.dealspr.on('pointertap', this.onClick, this);
        const poolspr = new Sprite(this.pool);
        poolspr.x = 3*(window.innerWidth)/6;
        poolspr.y = ((window.innerHeight)/3) -30;// + 100;
        poolspr.scale.x = 0.7;
        poolspr.scale.y = 0.7;
        poolspr.name = "pool";
        poolspr.interactive = true;
        poolspr.buttonMode = true;
        poolspr.on('pointertap', this.onClick, this);
        const pointspr = new Sprite(this.points);
        pointspr.x = 3*(window.innerWidth)/12;
        pointspr.y = ((window.innerHeight)/3) -30;
        pointspr.scale.x = 0.7;
        pointspr.scale.y = 0.7;
        pointspr.name = "point";
        pointspr.interactive = true;
        pointspr.buttonMode = true;
        pointspr.on('pointertap', this.onClick, this);
        this.cash = Texture.from(this.strBasePath + "cash.png");
        this.tournament = Texture.from(this.strBasePath + "tournament.png");
        this.practice = Texture.from(this.strBasePath + "practice.png");
        const cashspr = new Sprite(this.cash);
        cashspr.x = 0;
        cashspr.y = ((window.innerHeight)/4) + 30;
        cashspr.scale.x = 0.4;
        cashspr.scale.y = 0.4;
        cashspr.interactive = true;
        cashspr.buttonMode = true;
        cashspr.on('pointertap', this.onCash, this);
        const tournamentspr = new Sprite(this.tournament);
        tournamentspr.x = 0;
        tournamentspr.y = ((window.innerHeight)/2)- 35;
        tournamentspr.scale.x = 0.4;
        tournamentspr.scale.y = 0.4;
        tournamentspr.interactive = true;
        tournamentspr.buttonMode = true;
        tournamentspr.on('pointertap', this.onTournament, this);
        const practicespr = new Sprite(this.practice);
        practicespr.x = 0;
        practicespr.y = ((window.innerHeight)/2)+ 80;
        practicespr.scale.x = 0.4;
        practicespr.scale.y = 0.4;
        practicespr.interactive = true;
        practicespr.buttonMode = true;
        practicespr.on('pointertap', this.onPractice, this);
        this.addCash = Texture.from(this.strBasePath + "add-cash.png");
        const addCashspr = new Sprite(this.addCash);
        addCashspr.x = (window.innerWidth)-300;
        addCashspr.y = 13;// + 10;
        addCashspr.scale.x = 0.5;
        addCashspr.scale.y = 0.5;
        if(window.innerWidth < 992) {
            tournamentspr.y = ((window.innerHeight)/2);
        }
        this.funCoins = Texture.from(this.strBasePath + "fun_coins.png");
        const funCoinsspr = new Sprite(this.funCoins);
        funCoinsspr.x = (window.innerWidth)-200;
        funCoinsspr.y = 13;
        funCoinsspr.scale.x = 0.45;
        funCoinsspr.scale.y = 0.45;
        this.bonus = Texture.from(this.strBasePath + "bonus.png");
        const bonusspr = new Sprite(this.bonus);
        bonusspr.x = (window.innerWidth)-100;
        bonusspr.y = 13;
        bonusspr.scale.x = 0.45;
        bonusspr.scale.y = 0.45;
        this.user = Texture.from(this.strBasePath + "user_icon.png");
        const userspr = new Sprite(this.user);
        userspr.x = 5;
        userspr.y = 5;
        userspr.scale.x = 0.45;
        userspr.scale.y = 0.45;
        this.container.addChild(bunny);
        startContainer.addChild(this.dealspr);
        startContainer.addChild(poolspr);
        startContainer.addChild(pointspr);
        startContainer.addChild(cashspr);
        startContainer.addChild(tournamentspr);
        startContainer.addChild(practicespr);
        startContainer.addChild(addCashspr);
        startContainer.addChild(funCoinsspr);
        startContainer.addChild(bonusspr);
        startContainer.addChild(userspr);
        this.container.addChild(startContainer);
        this.strcurrencyType = "cash";
    }

    private onTournament(event) {
        console.log("event = " + event);
        this.strcurrencyType = "tournament";
    }

    private onPractice(event) {
        console.log("event = " + event);
        this.strcurrencyType = "practice";
    }

    private onCash(event) {
        console.log("event = " + event);
        this.strcurrencyType = "cash";
    }

    private onClick(evt) {
        var arrHeaderText: string[] = [];
        var arrDataGridHeaderText: string[] = [];
        arrHeaderText = ['My Account', 'Promotions', 'Bring-A-Friend', 'RPS'];
        if (evt.target.name == "deal") {
            arrDataGridHeaderText = ['Name', 'Deals', 'Max Players', 'Entry Fee', 'Prize', 'Active Players', 'Registering', 'Action']
            this.RenderLobbyTables(global.app.getGameConfig().getlobbyData(evt.target.name, this.strcurrencyType), "Deal", arrHeaderText, arrDataGridHeaderText);
        }
        if (evt.target.name == "pool") {
            arrDataGridHeaderText = ['Name', 'Type', 'Max Players', 'Entry Fee', 'Prize', 'Active Players', 'Registering', 'Action']
            this.RenderLobbyTables(global.app.getGameConfig().getlobbyData(evt.target.name, this.strcurrencyType), "Pool", arrHeaderText, arrDataGridHeaderText);
        }
        if (evt.target.name == "point") {
            arrDataGridHeaderText = ['Name', 'Decks', 'Max Players', 'Min- Entry', 'Prize', 'Active Players', 'Registering', 'Action']
            this.RenderLobbyTables(global.app.getGameConfig().getlobbyData(evt.target.name, this.strcurrencyType), "Point", arrHeaderText, arrDataGridHeaderText);
        }
    }

    private RenderLobbyTables(param, strType: string, arrHeader: string[] = [], arrDataGridHeader: string[] = []) {
        const startLobby = this.container.getChildByName("Start");
        startLobby.visible = false;
        if (this.container.getChildByName(strType) != null) {
            this.DataGrid = this.container.getChildByName(strType);
            this.DataGrid.visible = true;
            const home = this.container.getChildByName("home");
            home.visible = false;
            document.addEventListener('wheel', (this, this.scroll));
            return;
        }
        const DataGrid = new Container();
        DataGrid.name = strType + "-" + this.strcurrencyType;
        const Header = new Graphics();
        Header.beginFill(0x0c0015);//(0x0c0015);(0xDE3249)
        Header.drawRect(60, 137, (window.innerWidth)-140, 102);
        Header.endFill();
        Header.lineStyle(2, 0xFF00FF, 1);
        const HeaderH = new Graphics();
        HeaderH.beginFill(0x210237, 0.9);//0x650A5A 0x210237
        HeaderH.drawRoundedRect(60, 83, (window.innerWidth)-140, window.innerHeight-100, 14);
        HeaderH.endFill();
        DataGrid.addChild(HeaderH);
        const style = new TextStyle({ fill: '#f6ba32', fontWeight: 'bold', fontSize: 14 });
        //const style1 = new TextStyle({fill:'#ffffff',fontWeight: 'bold',fontSize: 13});
        const style2 = new TextStyle({ fill: '#ffffff', fontWeight: 'bold', fontSize: 24 });
        var arrHeaderXpos: number[] = [(window.innerWidth)/8, ((window.innerWidth)/3), (window.innerWidth)/1.75, (Header.width-150)];
        for (var j: number = 0; j < arrHeader.length; ++j) {
            const txtHeader = new Text(arrHeader[j], style2);
            txtHeader.x = arrHeaderXpos[j];
            txtHeader.y = 100;
            DataGrid.addChild(txtHeader);
        }
        DataGrid.addChild(Header);

        var arrXpos: number[] = [(Header.width)/14, (Header.width)/6, (Header.width)/4, (Header.width)/2.75, (Header.width)/2.1, (Header.width)/1.8, (Header.width)/1.4, (Header.width)/1.1];

        console.log(Header.width)
        for (var i: number = 0; i < arrDataGridHeader.length; ++i) {
            const text = new Text(arrDataGridHeader[i], style);
            text.x = arrXpos[i];
            text.y = 180;
            DataGrid.addChild(text);
        }
        this.home = Texture.from(this.strBasePath + "back-arrow.png");
        const homespr = new Sprite(this.home);
        homespr.name = "home";
        homespr.x = 20;
        homespr.y = 20;
        homespr.scale.x = 0.5;
        homespr.scale.y = 0.5;
        homespr.interactive = true;
        homespr.buttonMode = true;
        homespr.on('pointertap', this.MoveBack, this);
        this.RenderData(param, DataGrid, strType);
        this.container.addChild(homespr);
        this.container.addChild(DataGrid);
        this.DataGrid = DataGrid;
    }

    private MoveBack(event) {
        console.log("MoveBack event  = " + event);
        const startLobby = this.container.getChildByName("Start");
        startLobby.visible = true;
        this.DataGrid.visible = false;
        const home = this.container.getChildByName("home");
        home.visible = false;
        document.removeEventListener('wheel', (this, this.scroll));
    }

    private RenderData(param: [Games], DataGrid, strType: string) {
        const lobbyGrid = new Container();
        const style1 = new TextStyle({ fill: '#ffffff', fontWeight: 'bold', fontSize: 13 });
        
        const Mask = new Graphics();
        Mask.beginFill(0xffffff, 0.2);//0x650A5A 0x210237
        Mask.drawRect(60, 239, (window.innerWidth)-140, 397);
        Mask.endFill();
        lobbyGrid.name = "list";
        var arrXpos: number[] = [
            (Mask.width)/50, (Mask.width)/8, (Mask.width)/4.5, (Mask.width)/3, (Mask.width)/2.35, (Mask.width)/1.8, (Mask.width)/1.45, (Mask.width)/1.1];

        const scroll = new Graphics();
        scroll.beginFill(0xcccccc, 0.2);//0x650A5A 0x210237
        scroll.drawRect((window.innerWidth)-140, 239, 2, 397);//(1202, 241, 2, 395);
        scroll.endFill();
        const scrollbar = new Graphics();
        scrollbar.beginFill(0xcccccc, 0.8);//0x650A5A 0x210237
        scrollbar.drawRoundedRect(0, 0, 10, 40, 1)//(1096, 241, 10, 40, 1);//(1198, 241, 10, 40, 1);
        scrollbar.endFill();
        scrollbar.name = "ScrollBar";
        const scrollbarspr = new Container();
        scrollbarspr.addChild(scrollbar);
        scrollbarspr.x = (window.innerWidth)-140;
        scrollbarspr.y = Mask.height;
        scrollbarspr.interactive = true;
        scrollbarspr.buttonMode = true;
        scrollbarspr
            .on('pointertap', this.onDragStart, this)
            .on('pointerup', this.onDragEnd, this)
            .on('pointerupoutside', this.onDragEnd, this)
            .on('pointermove', this.onDragMove, this);

        lobbyGrid.mask = Mask;
        var arrText: string[];
        for (var i: number = 0; i < param.length; ++i) {
            const lobbyspr = new Container();

            if (strType == "Deals") {
                arrText = [param[i]["game_title"], param[i]["deals"].toString(),
                param[i]["seats"].toString(), param[i]["entry_fee"].toString(),
                param[i]["pool_deal_prize"].toString(), (i + 1).toLocaleString(), "0"];
            }
            else if (strType == "Pool") {
                arrText = [param[i]["game_title"], param[i]["pool_game_type"].toString(),
                param[i]["seats"].toString(), param[i]["entry_fee"].toString(),
                param[i]["pool_deal_prize"].toString(), (i + 1).toLocaleString(), "0"];
            }
            else {
                arrText = [param[i]["game_title"], param[i]["number_of_deck"].toString(),
                param[i]["seats"].toString(), param[i]["entry_fee"].toString(),
                param[i]["pool_deal_prize"].toString(), (i + 1).toLocaleString(), "0"];
            }
            const Header = new Graphics();
            lobbyspr.y = i * 51;
            lobbyspr.addChild(Header);
            for (var j: number = 0; j < arrText.length; ++j) {
                const text = new Text(arrText[j], style1);
                text.x = arrXpos[j];
                text.y = 20;
                lobbyspr.addChild(text);
            }
            var playNow = Texture.from(this.strBasePath + "play-now.png");
            const playNowspr = new Sprite(playNow);
            playNowspr.x =window.innerWidth * 0.77 //950;//1050;
            playNowspr.y = 15;
            playNowspr.scale.x = 0.45;
            playNowspr.scale.y = 0.45;
            playNowspr.interactive = true;
            playNowspr.buttonMode = true;
            var oToken: Object = new Object();
            oToken["token"] = param[i]["token"]
            oToken["type"] = param[i]["game_type"]
            playNowspr.on('pointertap', this.openGame, oToken);

            Header.beginFill(0x0c0015, ((i % 2) * 1));//(0x0c0015);(0xDE3249) ((i%2) * 1)
            Header.drawRect(0, 0, (window.innerWidth)-140, 51);
            Header.endFill();
            Header.lineStyle(2, 0xFF00FF, 1);

            lobbyspr.addChild(playNowspr);
            lobbyGrid.addChild(lobbyspr);

            if(window.innerWidth <= 1000) {
                playNowspr.x =window.innerWidth * 0.72
            }
        }
        lobbyGrid.x = 60;
        lobbyGrid.y = 239;
        document.addEventListener('wheel', (this, this.scroll), false);
        DataGrid.addChild(lobbyGrid);
        DataGrid.addChild(Mask);
        
    }

    private onDragStart(event) {
        document.addEventListener('mousemove', this.onMouseMove);        
        event.currentTarget.dragging = true;        
    }

    private scroll(event) {
        const lobby = this.DataGrid.getChildByName("list");
        if (event.clientY > 239 && event.clientY < 239 + 397 && event.clientX > 60 && event.clientX < 60 + 1150) {
            if (event.deltaY < 0) {
                if (lobby.y < 239) {
                    lobby.y += 20;
                }
            }
            else if (event.deltaY > 0) {
                if (lobby.y + lobby.height - 397 > 239) {
                    lobby.y -= 20;
                }

            }
        }
    }

    private onDragEnd(event) {
        event.currentTarget.dragging = false;
        document.removeEventListener('mousemove', this.onMouseMove);
        //this.dragging = true;
    }

    private onMouseMove(event) {
        console.log("onMouseMove event.clientX  = " + event.clientX);
        this.mouseYPos = event.clientY;
        if (event.clientY > 239 && event.clientY < 239 + 397 && event.clientX > 60 && event.clientX < 60 + 1150) {
            document.addEventListener('wheel', (this, this.scroll), false);
        }
        else {
            document.removeEventListener('wheel', (this, this.scroll), false);
        }
    }
    private onDragMove(event) {
        if (event.currentTarget.dragging) {
            if (this.mouseYPos > 239 && this.mouseYPos < (239 + 357)) {
                event.currentTarget.y = this.mouseYPos;
                const lobby = event.currentTarget.parent.getChildByName("list");
                lobby.y = (239) - (2 * ((event.currentTarget.y - 239) * (lobby.height + 397) / (lobby.height + 40)));
            }
        }
    }

    private openGame(event)
    {
        console.log("event = " + event);
        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://rummydesk.com/api/playnow_token?'+'access_token='+global.app.getAccessToken(), true);
        xhr.onload = function() { global.app.getGameConfig().getLobby().openGameWindow(JSON.parse(this['responseText'])); };
        var formData = new FormData();
        formData.append("access_token", global.app.getAccessToken());
        xhr.send(formData);
        global.app.getGameConfig().getLobby().current_token = this["token"];
        global.app.getGameConfig().getLobby().current_type = this["type"];
    }

    public openGameWindow(evtParams)
    {
        global.app.OpenWindow("http://127.0.0.1:4200/?" + evtParams.data.access_token +"&" + evtParams.data.play_token + "&" +global.app.getGameConfig().getLobby().current_token +"&"+global.app.getGameConfig().getLobby().current_type);
        // global.app.OpenWindow("http://rummy.duoex.com/html5/www/index.html?" + evtParams.data.access_token +"&" + evtParams.data.play_token + "&" +global.app.getGameConfig().getLobby().current_token +"&"+global.app.getGameConfig().getLobby().current_type);
    }
}

