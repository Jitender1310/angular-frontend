import { Container, Sprite, Texture, Graphics, Text, TextStyle } from "pixi.js";
import { TextInput } from "../component/TextInput"
import { Register } from "./Register";
import * as global from "app";

export class Login {

  private strBasePath:string; 

  constructor()
  {
    this.strBasePath = global.app.getBasePath();
  }

  public loadView() {
    try {
      if (global.app.getContainer().getChildByName("Start") != null) {
        global.app.getContainer().getChildByName("Start").visible = false;
      }
      if (global.app.getContainer().getChildByName("Login") != null) {
        global.app.getContainer().getChildByName("Login").visible = true;
        return;
      }
    }
    catch (e) { }
    const loginContainer = new Container();
    loginContainer.name = "Login";

    const background = new Sprite(Texture.from(this.strBasePath + "background.png"));
    background.scale.x = 0.67;
    background.scale.y = 0.67;
    background.width = window.innerWidth;
    background.height = window.innerHeight;

    const HeaderH = new Graphics();
    HeaderH.beginFill(0x51178a, 0.9);
    HeaderH.drawRoundedRect((window.innerWidth) / 4, (window.innerHeight) / 4, (window.innerWidth) / 2, (window.innerHeight) / 2, 14);
    HeaderH.endFill();

    const usernameInput = new TextInput({
      input: {
        width: '30%',
        height: '4%',
        fontSize: '12px',
        color: '#29163f',
        padding: '8px',
        radius: '12px'
      },
      box: {
        default: {
          fill: 0xEEEEEE,
          rounded: 16
        },
        focused: { fill: 0xE1E3EE, rounded: 16, stroke: { color: 0xABAFC6, width: 4 } },
      },
      type: "text"
    })
    usernameInput.x = (window.innerWidth / 3);
    usernameInput.y = (window.innerHeight / 3);
    usernameInput.placeholder = 'Email / Username / PhoneNumber';

    const passwordInput = new TextInput({
      input: {
        width: '30%',
        height: '4%',
        fontSize: '12px',
        color: '#29163f',
        padding: '8px',
        radius: '12px'
      },
      box: {
        default: {
          fill: 0xEEEEEE,
          rounded: 16
        },
        focused: { fill: 0xE1E3EE, rounded: 16, stroke: { color: 0xABAFC6, width: 4 } },
      },
      type: "password"
    })
    passwordInput.x = (window.innerWidth / 3);
    passwordInput.y = (window.innerHeight / 3) + 60;
    passwordInput.placeholder = 'Enter Password';


    const loginImg = new Sprite(Texture.from(this.strBasePath + "login.png"));
    loginImg.x = (window.innerWidth / 3) + 30;
    loginImg.y = (window.innerHeight) / 2 + 30;
    loginImg.buttonMode = true;
    loginImg.interactive = true;
    loginImg.scale.x = 0.8;
    loginImg.scale.y = 0.8;

    const registerImg = new Sprite(Texture.from(this.strBasePath + "register.png"));
    registerImg.x = (window.innerWidth / 2);
    registerImg.y = (window.innerHeight) / 2  + 30;
    registerImg.buttonMode = true;
    registerImg.interactive = true;
    registerImg.scale.x = 0.8;
    registerImg.scale.y = 0.8;
    
    let style = new TextStyle({
      fontFamily: "Arial",
      fontSize: 20,
      fill: "red",
      dropShadow: true,
      dropShadowColor: "#000000",
      dropShadowBlur: 4,
      dropShadowAngle: Math.PI / 6,
      dropShadowDistance: 6,
    });
    
    let errorText = new Text("", style);
    errorText.x = (window.innerWidth / 2) - 100;
    errorText.y = ((window.innerHeight) / 2) + 60;
    errorText.visible = false;

    const fbImg = new Sprite(Texture.from(this.strBasePath+"facebook.png"));
    fbImg.width = 30;
    fbImg.height = 30;
    fbImg.buttonMode = true;
    fbImg.interactive = true;
    fbImg.x = (window.innerWidth / 2) - 50;
    fbImg.y = (window.innerHeight) / 1.5;

    const gImg = new Sprite(Texture.from(this.strBasePath + "gmail.png"));
    gImg.width = 30;
    gImg.height = 30;
    gImg.buttonMode = true;
    gImg.interactive = true;
    gImg.x = (window.innerWidth / 2);
    gImg.y = (window.innerHeight) / 1.5;

    loginImg.on('pointertap', () => {
      if (usernameInput.text == "" || passwordInput.text == "") {
        errorText.text = "Please enter all fields.";
        errorText.visible = true;
      } else {
        // global.app.getGameConfig().Login(usernameInput.text, passwordInput.text, global.app.getGameConfig().WebLoginRes)
        global.app.getGameConfig().Login(usernameInput.text, passwordInput.text, res => {
            if (res.status == "valid") {
                localStorage.setItem("access_token", res.data.access_token);
                localStorage.setItem("username", res.data.username);
                global.app.getGameConfig().WebLoginRes(res);
            } else {                
                errorText.text = res.message;
                errorText.visible = true;
            }
        });
      }
    });

    registerImg.on('pointertap', () => {
      // global.app.getStage().addChild(this.loadGame());
      let register = new Register();
      register.loadView();
    });


    loginContainer.addChild(background);
    loginContainer.addChild(HeaderH);

    loginContainer.addChild(usernameInput);
    loginContainer.addChild(passwordInput);

    loginContainer.addChild(loginImg);
    loginContainer.addChild(registerImg);
    loginContainer.addChild(errorText);
    loginContainer.addChild(fbImg);
    loginContainer.addChild(gImg);
    
    global.app.getContainer().addChild(loginContainer);
    //global.app.getGameConfig().Login("leazo", "leazo123", global.app.getGameConfig().WebLoginRes)
    //return loginContainer;
  }
}

