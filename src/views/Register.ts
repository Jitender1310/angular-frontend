import { Container, Sprite, Texture, Graphics, Text, TextStyle } from "pixi.js";
import { TextInput } from "../component/TextInput"
import * as global from "app";

export class Register {

    private strBasePath:string; 

    constructor()
    {
      this.strBasePath = global.app.getBasePath();
    }

    public loadView() {
        try {
            if (global.app.getContainer().getChildByName("Login") != null) {
                global.app.getContainer().getChildByName("Login").visible = false;
            }
        }
        catch (e) { }
        const loadGameContainer = new Container();
        loadGameContainer.name = "Start";

        const background = new Sprite(Texture.from( this.strBasePath + "background.png"));
        background.scale.x = 0.67;
        background.scale.y = 0.67;
        background.width = window.innerWidth;
        background.height = window.innerHeight;

        const HeaderH = new Graphics();
        HeaderH.beginFill(0x51178a, 0.9);
        HeaderH.drawRoundedRect((window.innerWidth) / 4, (window.innerHeight) / 8, (window.innerWidth) / 2, ((window.innerHeight) * 2 / 3), 14);
        HeaderH.endFill();

        const usernameInput = new TextInput({
            input: {
                width: '30%',
                height: '4%',
                fontSize: '12px',
                color: '#29163f',
                padding: '8px',
                radius: '12px'
            },
            box: {
                default: {
                    fill: 0xEEEEEE,
                    rounded: 16
                },
                focused: { fill: 0xE1E3EE, rounded: 16, stroke: { color: 0xABAFC6, width: 4 } },
            },
            type: "text"
        })
        usernameInput.x = (window.innerWidth / 3);
        usernameInput.y = (window.innerHeight / 3) * 0.8 -30;
        usernameInput.placeholder = 'Enter User Name';

        const useremailInput = new TextInput({
            input: {
                width: '30%',
                height: '4%',
                fontSize: '12px',
                color: '#29163f',
                padding: '8px',
                radius: '12px'
            },
            box: {
                default: {
                    fill: 0xEEEEEE,
                    rounded: 16
                },
                focused: { fill: 0xE1E3EE, rounded: 16, stroke: { color: 0xABAFC6, width: 4 } },
            },
            type: "text"
        })
        useremailInput.x = (window.innerWidth / 3);
        useremailInput.y = (window.innerHeight / 3) * 0.8 + 30;
        useremailInput.placeholder = 'Enter Email';

        const userphoneInput = new TextInput({
            input: {
                width: '30%',
                height: '4%',
                fontSize: '12px',
                color: '#29163f',
                padding: '8px',
                radius: '12px'
            },
            box: {
                default: {
                    fill: 0xEEEEEE,
                    rounded: 16
                },
                focused: { fill: 0xE1E3EE, rounded: 16, stroke: { color: 0xABAFC6, width: 4 } },
            },
            type: "text"
        })
        userphoneInput.x = (window.innerWidth / 3);
        userphoneInput.y = (window.innerHeight / 3) * 0.8 + 90;
        userphoneInput.placeholder = 'Enter Phone number';


        const passwordInput = new TextInput({
            input: {
                width: '30%',
                height: '4%',
                fontSize: '12px',
                color: '#29163f',
                padding: '8px',
                radius: '12px'
            },
            box: {
                default: {
                    fill: 0xEEEEEE,
                    rounded: 16
                },
                focused: { fill: 0xE1E3EE, rounded: 16, stroke: { color: 0x29163f, width: 4 } },
            },
            type: "password"
        })
        passwordInput.x = (window.innerWidth / 3);
        passwordInput.y = (window.innerHeight / 3) * 0.8 + 150;
        passwordInput.placeholder = 'Enter Password';


        const registerImg = new Sprite(Texture.from(this.strBasePath + "register.png"));
        registerImg.x = (window.innerWidth / 2) - 80;
        registerImg.y = (window.innerHeight) * 2 / 3 -40;
        registerImg.buttonMode = true;
        registerImg.interactive = true;
        registerImg.scale.x = 0.8;
        registerImg.scale.y = 0.8;

        let style = new TextStyle({
            fontFamily: "Arial",
            fontSize: 20,
            fill: "white",
            stroke: '#ff3300',
            strokeThickness: 4,
            dropShadow: true,
            dropShadowColor: "#000000",
            dropShadowBlur: 4,
            dropShadowAngle: Math.PI / 6,
            dropShadowDistance: 6,
        });

        let regerrorText = new Text("Please enter all fields.", style);
        regerrorText.x = 300;
        regerrorText.y = 500;
        regerrorText.visible = false;

        const fbImg = new Sprite(Texture.from(this.strBasePath +  "facebook.png"));
        fbImg.width = 30;
        fbImg.height = 30;
        fbImg.buttonMode = true;
        fbImg.interactive = true;
        fbImg.x = (window.innerWidth / 2) - 50;
        fbImg.y = (window.innerHeight) / 1.5 + 60;

        const gImg = new Sprite(Texture.from(this.strBasePath + "gmail.png"));
        gImg.width = 30;
        gImg.height = 30;
        gImg.buttonMode = true;
        gImg.interactive = true;
        gImg.x = (window.innerWidth / 2);
        gImg.y = (window.innerHeight) / 1.5 + 60;

        registerImg.on('pointertap', () => {
            // global.app.getStage().addChild(this.boardGame());
        });
        if (window.innerWidth < 992) {

            HeaderH.drawRoundedRect((window.innerWidth) / 4, (window.innerHeight) * 0.8 / 5, (window.innerWidth) / 2, ((window.innerHeight) * 0.8 + 100), 14);
            HeaderH.scale.y = 1.1;
            registerImg.x = (window.innerWidth / 2) - 80;
            registerImg.y = (window.innerHeight) * 2 / 3 + 60;
        }


        loadGameContainer.addChild(background);
        loadGameContainer.addChild(HeaderH);
        loadGameContainer.addChild(usernameInput);
        loadGameContainer.addChild(passwordInput);
        loadGameContainer.addChild(useremailInput);
        loadGameContainer.addChild(userphoneInput);
        loadGameContainer.addChild(registerImg);
        loadGameContainer.addChild(fbImg)
        loadGameContainer.addChild(gImg)
        loadGameContainer.addChild(regerrorText);


        global.app.getContainer().addChild(loadGameContainer);
        // return loadGameContainer;
    }
}

