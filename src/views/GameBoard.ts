import { Container, Sprite, Texture, Graphics, Text, TextStyle } from "pixi.js";
import * as global from "app";

export class GameBoard {
    private loadGameContainer;
  private arrSuit = ["DIAMOND", "CLUB", "SPADE", "HEART", "JOKER"]
  private arrRank = [1,2,3,4,5,6,7,8,9,10,11,12,13,14]
  private arrMeldPos = [230,450,670,890]
  private arrPlayerCards;
  private mapPlayerCards:Map<Object,Container>;
  private arrPlayerCardSprite;
  private txtTimer;
  private bSortedCards:boolean = false;
  private bCardPicked:boolean = false;
  private bMelded:boolean = false;
  private oGameJoker;
  private arrArrangedCards;
  private oGameCards;
  private oMeldOpenCard;
  private oCloseCardSlot;
  private oDiscardCard;
  private oGroupsofCard;
  private bGroupCards:boolean = false;
  private bCardDiscarded:boolean = true;
  private btnDiscard;
  private btnMeld;
  private oGroupCard;
  private nTimerCounter;
  private oGameTimer;
  private arrMeldGroupCards;
  private oClosedDeckCard;
  private oOpenCard;
  private oCloseCard;
  private oPickedCard;
  private oOpenDeckCard;
  private nNextXPos;
  private arrCardsAss = [["Diamond/A-diamond.png","Diamond/2-diamond.png","Diamond/3-diamond.png",
  "Diamond/4-diamond.png","Diamond/5-diamond.png", "Diamond/6-diamond.png", "Diamond/7-diamond.png",
  "Diamond/8-diamond.png", "Diamond/9-diamond.png", "Diamond/10-diamond.png", "Diamond/J-diamond.png",
  "Diamond/Q-diamond.png", "Diamond/K-diamond.png"],
  ["Club/A-clubs.png","Club/2-clubs.png","Club/3-clubs.png",
  "Club/4-clubs.png","Club/5-clubs.png", "Club/6-clubs.png", "Club/7-clubs.png",
  "Club/8-clubs.png", "Club/9-clubs.png", "Club/10-clubs.png", "Club/J-clubs.png",
  "Club/Q-clubs.png", "Club/K-clubs.png"],
  ["Spade/A-spade.png","Spade/2-spade.png","Spade/3-spade.png",
  "Spade/4-spade.png","Spade/5-spade.png", "Spade/6-spade.png", "Spade/7-spade.png",
  "Spade/8-spade.png", "Spade/9-spade.png", "Spade/10-spade.png", "Spade/J-spade.png",
  "Spade/Q-spade.png", "Spade/K-spade.png"],
  ["Heart/A-hearts.png","Heart/2-hearts.png","Heart/3-hearts.png",
  "Heart/4-hearts.png","Heart/5-hearts.png", "Heart/6-hearts.png", "Heart/7-hearts.png",
  "Heart/8-hearts.png", "Heart/9-hearts.png", "Heart/10-hearts.png", "Heart/J-hearts.png",
  "Heart/Q-hearts.png", "Heart/K-hearts.png"],["Joker.png"]];

  private strBasePath:string;
  // private tablespr: any; 

  constructor()
  {
    this.strBasePath = global.app.getBasePath();
  }


    public loadView() {
        this.loadGameContainer = new Container();
        this.loadGameContainer.name = "Start";
    
        const bgspr = new Sprite(Texture.from(this.strBasePath + "background.png"));//game_play_non_player.jpg"));//background.png"));
        bgspr.scale.x = 0.5;
        bgspr.scale.y = 0.5;
        bgspr.width= window.innerWidth;
        bgspr.height = window.innerHeight;
    
        const tablespr = new Sprite(Texture.from(this.strBasePath + "table_board.png"));
        tablespr.width = (window.innerWidth * 0.8);
        tablespr.height = (window.innerHeight * 0.8);
        tablespr.anchor.set(0.5);
        tablespr.x = (window.innerWidth/2);
        tablespr.y = (window.innerHeight/2) + 25;
    
        const sprTimer = new Sprite(Texture.from(this.strBasePath + "game_timer.png"));//game_play_non_player.jpg"));//background.png"));
        sprTimer.scale.x = 0.28;
        sprTimer.scale.y = 0.28;
        sprTimer.x = window.innerWidth-160;
        sprTimer.y =13;
    
        const sprCoins = new Sprite(Texture.from(this.strBasePath + "fun_coins.png"));//game_play_non_player.jpg"));//background.png"));
        sprCoins.scale.x = 0.35;
        sprCoins.scale.y = 0.35;
        sprCoins.x = window.innerWidth/3 -30;
        sprCoins.y =13;
    
        const sprChips = new Sprite(Texture.from(this.strBasePath + "fun_coins.png"));//game_play_non_player.jpg"));//background.png"));
        sprChips.scale.x = 0.35;
        sprChips.scale.y = 0.35;
        sprChips.x = (window.innerWidth/3) *2;
        sprChips.y =13;

        const sprSetting = new Sprite(Texture.from(this.strBasePath + "setting_button.png"));//game_play_non_player.jpg"));//background.png"));
        sprSetting.scale.x = 0.8;
        sprSetting.scale.y = 0.8;
        sprSetting.x = window.innerWidth-60;
        sprSetting.y =80;

        const sprPower = new Sprite(Texture.from(this.strBasePath + "power_button.png"));//game_play_non_player.jpg"));//background.png"));
        sprPower.scale.x = 0.8;
        sprPower.scale.y = 0.8;
        sprPower.x = window.innerWidth-60;
        sprPower.y =150;
    
        const HeaderH = new Graphics();
        HeaderH.lineStyle(2, 0xFFFFFF, .5);
        HeaderH.beginFill(0x29163f, 1);
        HeaderH.drawRoundedRect(12, 2, window.innerWidth-20, 50, 8);
        HeaderH.endFill();
        const style = new TextStyle({ fill: '#ffffff', fontWeight: 'bold', fontSize: 16 });
    
        var Gameid_text = new Text("Game ID:", style);
        Gameid_text.x = 26;
        Gameid_text.y = 20;
    
        this.nTimerCounter = 0;
        this.txtTimer = new Text("Time: 00:00:00", style);
        this.txtTimer.x = window.innerWidth-130;
        this.txtTimer.y = 20;
    
        var Round_text = new Text("Round(s):", style);
        Round_text.x = (window.innerWidth/5);
        Round_text.y = 20;
    
        this.loadGameContainer.addChild(bgspr);
        this.loadGameContainer.addChild(tablespr);
        this.loadGameContainer.addChild(HeaderH);
        this.loadGameContainer.addChild(Gameid_text);
        this.loadGameContainer.addChild(Round_text);
        this.loadGameContainer.addChild(sprTimer);
        this.loadGameContainer.addChild(this.txtTimer);   
        this.loadGameContainer.addChild(sprCoins);    
        this.loadGameContainer.addChild(sprChips);    
        this.loadGameContainer.addChild(sprPower);  
        this.loadGameContainer.addChild(sprSetting);
        //this.OpenMeldPopUp(new Object());
        global.app.getContainer().addChild(this.loadGameContainer);
        return this.loadGameContainer;
    }

    public AddPlayerImage(type:string)
    {
      const style = new TextStyle({fill:'#ffffff',fontWeight: 'bold',fontSize: 20});
      if(type == "Male")
      {
        const bgspr = new Sprite(Texture.from(this.strBasePath + "avatar_1_new.png"));//game_play_non_player.jpg"));//background.png"));
        bgspr.scale.x = 0.7;
        bgspr.scale.y = 0.7;
        bgspr.x = window.innerWidth/2 -80;
        bgspr.y = window.innerHeight-80;
        bgspr.anchor.set(0.5)
        this.loadGameContainer.addChild(bgspr);
      }
      else{
        const bgspr = new Sprite(Texture.from(this.strBasePath + "avatar-girl.png"));//game_play_non_player.jpg"));//background.png"));
        bgspr.scale.x = 0.5;
        bgspr.scale.y = 0.5;
        bgspr.x = (window.innerWidth)/2;
        bgspr.y = 80;
        this.loadGameContainer.addChild(bgspr);
      }
      const HeaderH = new Graphics();
      HeaderH.lineStyle(1, 0xFFFFFF, .5);
      HeaderH.beginFill(0xFFFFFF, .2);
      HeaderH.drawRoundedRect(700, 670, 95, 50, 5);
      HeaderH.endFill();
      this.loadGameContainer.addChild(HeaderH);
  
      var name_text = new Text(global.app.getGameConfig().name, style);
      name_text.x = 715;
      name_text.y = 685;
      this.loadGameContainer.addChild(name_text);
      var game_name = new Text(global.app.getGameConfig().game_sub_type, {fill:'#ffffff',fontWeight: 'bold',fontSize: 16});
      game_name.x = 675;
      game_name.y = 20;
      this.loadGameContainer.addChild(game_name);
    }
  
    public WaitingForPlayers()
    {
      try{
        if(this.loadGameContainer.getChildByName("Waitforplayers")!= null)
        {
          this.loadGameContainer.getChildByName("Waitforplayers").visible = true;
          return;
        }
      }
      catch(e){}
      const waitingContainer = new Container();
      waitingContainer.name = "Waitforplayers";
      const style = new TextStyle({fill:'#000000',fontWeight: 'bold',fontSize: 20});
      const HeaderH = new Graphics();
      HeaderH.lineStyle(1, 0xFFFFFF, 1);
      HeaderH.beginFill(0xFFFFFF, .2);
      HeaderH.drawRoundedRect(0, 0, 300, 50, 10);
      HeaderH.endFill();
      waitingContainer.addChild(HeaderH);
  
      var WaitingForPlayers_text = new Text("Waiting for players", style);
      WaitingForPlayers_text.x = 60;
      WaitingForPlayers_text.y = 13;
      waitingContainer.addChild(WaitingForPlayers_text);
      waitingContainer.x = (window.innerWidth/2)-150;
      waitingContainer.y = (window.innerHeight/2)-25;
      this.loadGameContainer.addChild(waitingContainer);
    }
  
    public WaitingForGame(timer:number)
    {
      try{
        if(this.loadGameContainer.getChildByName("Waitforplayers")!= null)
        {
          this.loadGameContainer.getChildByName("Waitforplayers").visible = false;
        }
      }
      catch(e){}
      const waitingContainer = new Container();
      waitingContainer.name = "GameTimer";
      const style = new TextStyle({fill:'#000000',fontWeight: 'bold',fontSize: 25});
      const HeaderH = new Graphics();
      HeaderH.lineStyle(1, 0xFFFFFF, 1);
      HeaderH.beginFill(0xFFFFFF, .2);
      HeaderH.drawRoundedRect(0, 0, 440, 50, 10);
      HeaderH.endFill();
      waitingContainer.addChild(HeaderH);
      var counter = timer;
      var interval = setInterval(() => {
        counter--;
        WaitingForPlayers_text.text = "Game will start in " + counter+" seconds";
        if(counter <= 0 ){
          waitingContainer.visible = false;
          clearInterval(interval);
        };
      }, 1000);
      var WaitingForPlayers_text = new Text("Game will start in " + counter+" seconds", style);
      WaitingForPlayers_text.x = 50;
      WaitingForPlayers_text.y = 10;    
      waitingContainer.addChild(WaitingForPlayers_text);
      waitingContainer.x = (window.innerWidth/2)-220;
      waitingContainer.y = (window.innerHeight/2)-25;
      this.loadGameContainer.addChild(waitingContainer);
    }
    public AddUsersToTable(chairList)
    {
      // console.log("chairList");
      for(var i:number = 0; i < chairList.length; ++i)
      {
        // console.log("chairList = " +chairList[i]);
        try{
          // console.log("chairList [userDetails] = " +chairList[i]["player"]["userDetails"]);
          // console.log("chairList [username] = " +chairList[i]["player"] ["userDetails"]["username"]);
          if(chairList[i]["player"]["userDetails"]["username"] != global.app.getGameConfig().name)
          {
            this.AddUserToTablePlayer(chairList[i]["player"]["userDetails"]["username"]);
          }
        }catch(e){}
      }
    }
    public AddUserToTablePlayer(username:string)
    {
      const bgspr = new Sprite(Texture.from(this.strBasePath + "avatar-3_new.png"));
      bgspr.scale.x = 0.5;
      bgspr.scale.y = 0.5;
      bgspr.x = window.innerWidth/2 -80;
      bgspr.y = 50;
      
      const style = new TextStyle({fill:'#ffffff',fontWeight: 'bold',fontSize: 20});
      var name_text = new Text(username, style);
      name_text.x =  window.innerWidth/2;
      name_text.y = 60;
      const HeaderH = new Graphics();
      HeaderH.lineStyle(1, 0xFFFFFF, .5);
      HeaderH.beginFill(0xFFFFFF, .2);
      HeaderH.drawRoundedRect( window.innerWidth/2, 60, 95, 50, 5);
      //HeaderH.drawRoundedRect(687, 100, 95, 100, 5);
      HeaderH.endFill();
      this.loadGameContainer.addChild(bgspr);
      this.loadGameContainer.addChild(HeaderH);
      this.loadGameContainer.addChild(name_text);
    }
  
    public CardClicked(evtparams)
    {
      console.log("Got evtparams " + evtparams);
      var oGameView = global.app.getGameConfig().getGameBoard();
      var oCard;
      for(var i = 0; i < oGameView.arrPlayerCards.length; ++i)
      {
        if(this == oGameView.arrPlayerCards[i])
        {
          oCard = oGameView.mapPlayerCards.get(oGameView.arrPlayerCards[i]);
          // console.log("Got Card " + oCard);
          break;
        }
      }  
      // console.log("oGameView.oGroupCard.length " + oGameView.oGroupCard.length);
      // console.log("oCard " + oCard);
      var bGot:boolean = false;
      for(i = 0; i < oGameView.oGroupCard.length; ++i)
      {
        if(oGameView.oGroupCard[i] == this)
        {
          oGameView.oGroupCard.splice(i, 1);
          //oCard.tint = 0xFFFFFF;
          for(var j = 0; j < oCard.children.length;++j)
          {
            var oChild:Sprite = oCard.getChildAt(j) as Sprite;
            oChild.tint = 0xFFFFFF;
          }
          bGot = true;
          break;
        }
      }
      if(!bGot)
      {
        oGameView.oGroupCard.push(this);
        for(var j = 0; j < oCard.children.length;++j)
        {
          var oChild:Sprite = oCard.getChildAt(j) as Sprite;
          oChild.tint = 0xCCCCCC;
        }
        //oCard.tint = 0xCCCCCC;   
      }
       console.log("oGameView.oGroupCard.length after " + oGameView.oGroupCard.length);
       console.log("oGameView.bCardPicked = " + oGameView.bCardPicked);
      var cPlayerCards = oGameView.loadGameContainer.getChildByName("PlayerCards");
      console.log("Got cPlayerCards " + cPlayerCards);
      if(oGameView.oGroupCard.length > 1)
      {      
        //cPlayerCards.getChildByName("discard").visible = oGameView.bCardPicked;
        oGameView.oDiscardCard = null;
      }
      else if(oGameView.oGroupCard.length == 1){
        oGameView.oDiscardCard = oGameView.oGroupCard[0];
        // cPlayerCards.getChildByName("discard").visible = oGameView.bCardPicked;
      }
      else{
        oGameView.oDiscardCard = null;
        // cPlayerCards.getChildByName("discard").visible = oGameView.bCardPicked;
      }
    }
  
    public ViewGameCards(oGameCards)
    {
      console.log("ViewGameCards oGameCards " + oGameCards);
      var cGameCards;
      this.oGameCards = oGameCards;
      if(this.loadGameContainer.getChildByName("GameCards") != null)
      {
        cGameCards = this.loadGameContainer.getChildByName("GameCards");
      }
      else{
        cGameCards = new Container();
        cGameCards.name = "GameCards";
      }
      //cGameCards = new Container();
      //cGameCards.name = "GameCards";
      var oCutJoker = oGameCards.cutJoker;
      var oOpenDeck = oGameCards.nextCard_In_OpenDeck_Top1;
      var oClosedDeck = oGameCards.nextCard_In_ClosedDeck;
      var strPath = this.GetCardPath(oCutJoker);
      const sprCutJoker = new Sprite(Texture.from(strPath));//game_play_non_player.jpg"));//background.png"));
      sprCutJoker.name = "JokerCard";
      sprCutJoker.scale.x = 0.7;
      sprCutJoker.scale.y = 0.7;
      sprCutJoker.x = window.innerWidth/4;
      sprCutJoker.y = window.innerHeight/2.45;
      this.oGameJoker = oCutJoker;
      sprCutJoker.anchor.set(0.5);
      sprCutJoker.rotation = 1.570;
      cGameCards.addChild(sprCutJoker);
  
      strPath = this.GetCardPath(oOpenDeck);
      this.oOpenDeckCard = new Sprite(Texture.from(strPath));//game_play_non_player.jpg"));//background.png"));
      this.oOpenDeckCard.name = "OpenDeck";
      this.oOpenDeckCard.scale.x = 0.7;
      this.oOpenDeckCard.scale.y = 0.7;
      this.oOpenDeckCard.x = window.innerWidth/4;
      this.oOpenDeckCard.y = window.innerHeight/3;
      this.oOpenCard = oOpenDeck;
      cGameCards.addChild(this.oOpenDeckCard);
      this.oOpenDeckCard.on('pointerdown', this.TakeOpenCard, this);

      this.oCloseCardSlot = new Graphics();
      this.oCloseCardSlot.beginFill(0x333333, 0.8);
      this.oCloseCardSlot.drawRoundedRect(850, 240, this.oOpenDeckCard.width, this.oOpenDeckCard.height, 14);
      this.oCloseCardSlot.endFill();
      this.oCloseCardSlot.visible = false;
      //this.oCloseCardSlot.x = (window.innerWidth / 8);
      //this.oCloseCardSlot.y = window.innerHeight / 16 - 25;
      //this.oCloseCardSlot.height = 100;
      //this.oCloseCardSlot.width = 75;
  
      strPath = this.strBasePath + "closedDeck.png"
      this.oClosedDeckCard = new Sprite(Texture.from(strPath));//game_play_non_player.jpg"));//background.png"));
      this.oClosedDeckCard.name = "CloseDeck";
      this.oClosedDeckCard.scale.x = 0.7;
      this.oClosedDeckCard.scale.y = 0.7;
      this.oClosedDeckCard.x = window.innerWidth/4;
      this.oClosedDeckCard.y = window.innerHeight/3;
      this.oCloseCard = oClosedDeck;
      cGameCards.addChild(this.oClosedDeckCard);
      cGameCards.addChild(this.oCloseCardSlot);
      this.oClosedDeckCard.on('pointerdown', this.TakeCloseCard, this);
      this.loadGameContainer.addChild(cGameCards);
    }
  
    private GetCardPath(oCard):string
    {
      var strPath = this.strBasePath;
      for(var j:number = 0; j < this.arrSuit.length; ++j)
      {      
        if(oCard.suit == this.arrSuit[j])
        {
          for(var k:number = 0; k < this.arrRank.length; ++k)
          {
            if(oCard.rank == 0)
            {
              strPath += this.arrCardsAss[j][0];
              break;
            }
            if(oCard.rank == this.arrRank[k])
            {
              // console.log("this.arrCardsAss[j][k] = " + this.arrCardsAss[j][k]);
              strPath += this.arrCardsAss[j][k];
              // console.log("strPath = " + strPath);
              break;
            }
          }
          break;
        }      
      }
      return strPath;
    }
  
    public ChangeGameCards(oGameCards, bTurn:boolean = false)
    {
      var strPath = "";
      this.oGameCards = oGameCards;
      var cGameCards = this.loadGameContainer.getChildByName("GameCards");
      try
      {
        cGameCards.removeChild(this.oOpenDeckCard);
        cGameCards.removeChild(this.oClosedDeckCard);
      }catch(e){}
      var oOpenDeck = oGameCards.nextCard_In_OpenDeck_Top1;
      var oClosedDeck = oGameCards.nextCard_In_ClosedDeck;
      strPath = this.GetCardPath(oOpenDeck);
      this.oOpenDeckCard = new Sprite(Texture.from(strPath));//game_play_non_player.jpg"));//background.png"));
      this.oOpenDeckCard.name = "OpenDeck";
      this.oOpenDeckCard.scale.x = 0.7;
      this.oOpenDeckCard.scale.y = 0.7;
      this.oOpenDeckCard.x = window.innerWidth/2;
      this.oOpenDeckCard.y = window.innerHeight/3;    
      this.oOpenCard = oOpenDeck;
      this.oOpenDeckCard.on('pointerdown', this.TakeOpenCard, this);
      cGameCards.addChild(this.oOpenDeckCard);
      strPath = this.strBasePath + "closedDeck.png"
      this.oClosedDeckCard = new Sprite(Texture.from(strPath));//game_play_non_player.jpg"));//background.png"));
      this.oClosedDeckCard.name = "CloseDeck";
      this.oClosedDeckCard.scale.x = 0.7;
      this.oClosedDeckCard.scale.y = 0.7;
      this.oClosedDeckCard.x = window.innerWidth/4;
      this.oClosedDeckCard.y = window.innerHeight/3.25;
      this.oCloseCard = oClosedDeck;
      this.oClosedDeckCard.on('pointerdown', this.TakeCloseCard, this);
      this.oOpenDeckCard.interactive = bTurn;        
      this.oOpenDeckCard.buttonMode = bTurn;    
      this.oClosedDeckCard.interactive = bTurn;        
      this.oClosedDeckCard.buttonMode = bTurn;
      cGameCards.addChild(this.oClosedDeckCard);
    }
  
    public ViewOpponentCutForSeatCard(oCard)
    {
      var cPlayerCards = this.loadGameContainer.getChildByName("CutForSeat");
      if(cPlayerCards == null || cPlayerCards == undefined)
      {
        cPlayerCards = new Container();
        cPlayerCards.name = "CutForSeat";
        this.GameTimer();
      }
      var strPath = this.GetCardPath(oCard);
      const bgspr = new Sprite(Texture.from(strPath));//game_play_non_player.jpg"));//background.png"));
      bgspr.name = oCard.index.toString();
      bgspr.scale.x = 0.7;
      bgspr.scale.y = 0.7;
      bgspr.x = 600;
      bgspr.y = 210;    
      cPlayerCards.addChild(bgspr);
      this.loadGameContainer.addChild(cPlayerCards);
    }
  
    public GameTimer()
    {
      if(this.oGameTimer == null)
      {
        this.oGameTimer = setInterval(() => {
          this.nTimerCounter+=1;
          this.txtTimer.text = "Time: 00:" + this.ConvertTimertoString(this.nTimerCounter);        
        }, 1000);
      }
    }
  
    public ViewMyCutForSeatCard(oCard)
    {
      var cPlayerCards = this.loadGameContainer.getChildByName("CutForSeat");
      if(cPlayerCards == null || cPlayerCards == undefined)
      {
        cPlayerCards = new Container();
        cPlayerCards.name = "CutForSeat";
        this.GameTimer();
      }
      
      var strPath = this.GetCardPath(oCard);
      const bgspr = new Sprite(Texture.from(strPath));
      bgspr.name = oCard.index.toString();
      bgspr.scale.x = 0.7;
      bgspr.scale.y = 0.7;
      bgspr.x = 600;
      bgspr.y = 460;    
      cPlayerCards.addChild(bgspr);
      this.loadGameContainer.addChild(cPlayerCards);
    }
    private ConvertTimertoString(nTimer):string{
      var sec_num = parseInt(nTimer, 10)
      var hours   = Math.floor(sec_num / 3600)
      var minutes = Math.floor(sec_num / 60) % 60
      var seconds = sec_num % 60
  
  
      return [hours,minutes,seconds]
          .map(v => v < 10 ? "0" + v : v)
          .filter((v,i) => v !== "00" || i > 0)
          .join(":")
    }
  
    public ViewPlayerCards(arrCardList)
    {
      this.GameTimer();
      this.bSortedCards = false;
      this.bMelded = false;
      this.arrArrangedCards = new Array();
      this.oGroupsofCard = new Array();
      this.oGroupCard = new Array();
      this.arrPlayerCards = new Array();
      this.arrPlayerCardSprite = new Array();
      this.mapPlayerCards = new Map();
      var cPlayerCards;
      try{
        if(this.loadGameContainer.getChildByName("GameTimer")!= null)
        {
          this.loadGameContainer.getChildByName("GameTimer").visible = false;
        }
        if(this.loadGameContainer.getChildByName("CutForSeat")!= null)
        {
          this.loadGameContainer.getChildByName("CutForSeat").visible = false;
        }
        this.CloseDashboard();
      }catch(e){}
      var strPath:string;
      if(this.loadGameContainer.getChildByName("PlayerCards") != null)
      {
        cPlayerCards = this.loadGameContainer.getChildByName("PlayerCards");
      }
      else{
        cPlayerCards = new Container();
        cPlayerCards.name = "PlayerCards";
      }


      // Player Cards
      
      for(var i:number = 0; i < arrCardList.length; ++i)
      {
        strPath = this.GetCardPath(arrCardList[i]);
        var oCard = new Container();
        oCard.name = arrCardList[i].index.toString()+ "-" + arrCardList[i].suit.toString() +"-" +arrCardList[i].rank.toString();
        const sprCard = new Sprite(Texture.from(strPath));
        sprCard.name = arrCardList[i].index.toString();
        oCard.addChild(sprCard)
        if(arrCardList[i].isCutJoker)
        {
          const sprJoker = new Sprite(Texture.from(this.strBasePath + "card_joker.png"));
          sprJoker.scale.x = 0.1;
          sprJoker.scale.y = 0.1;
          sprJoker.x = 5;
          sprJoker.y = 160;
          oCard.addChild(sprJoker);
        }
        oCard.scale.x = 0.7;
        oCard.scale.y = 0.7;
        oCard.x = (window.innerWidth/6) + (i*30);
        oCard.y = (window.innerHeight/3) *2 -40;
        oCard.interactive = true;        
        oCard.buttonMode = true;
        oCard.on('pointerdown', this.CardClicked, arrCardList[i]);


        cPlayerCards.addChild(oCard);
        this.nNextXPos = (window.innerWidth/6) + ((i + 1)*30)
        this.arrPlayerCards.push(arrCardList[i]);
        this.arrPlayerCardSprite.push(oCard);
        this.mapPlayerCards.set(arrCardList[i], oCard);
      }
      
      const btnSortspr = new Sprite(Texture.from(this.strBasePath + "sort.png"));
      btnSortspr.x = window.innerWidth - 100;
      btnSortspr.y =window.innerHeight -150;
      btnSortspr.scale.x = 0.45;
      btnSortspr.scale.y = 0.45;
      btnSortspr.interactive = true;    
      btnSortspr.buttonMode = true;
      btnSortspr.on('pointerdown', this.SortPlayerCards, this);
  
      this.btnDiscard = new Sprite(Texture.from(this.strBasePath + "discard.png"));
      this.btnDiscard.name = "discard";
      this.btnDiscard.x = window.innerWidth - 100
      this.btnDiscard.y = window.innerHeight -200;
      this.btnDiscard.scale.x = 0.45;
      this.btnDiscard.scale.y = 0.45;
      this.btnDiscard.interactive = true;    
      this.btnDiscard.buttonMode = true;
      this.btnDiscard.visible = false;
      this.btnDiscard.on('pointerdown', this.DiscardPlayerCards, this);
      
  
      this.btnMeld = new Sprite(Texture.from(this.strBasePath + "finish.png"));
      this.btnMeld.name = "Meld";
      this.btnMeld.x = window.innerWidth - 100;
      this.btnMeld.y = window.innerHeight -85;
      this.btnMeld.scale.x = 0.45;
      this.btnMeld.scale.y = 0.45;
      this.btnMeld.interactive = true;    
      this.btnMeld.buttonMode = true;
      this.btnMeld.visible = false;
      this.btnMeld.on('pointerdown', this.OpenMeldPopUp, this);
  
      const btnGroupspr = new Sprite(Texture.from(this.strBasePath + "group.png"));
      btnGroupspr.name = "group";
      btnGroupspr.x = window.innerWidth - 100;
      btnGroupspr.y = window.innerHeight -120;
      btnGroupspr.scale.x = 0.45;
      btnGroupspr.scale.y = 0.45;
      btnGroupspr.interactive = true;    
      btnGroupspr.buttonMode = true;
      btnGroupspr.on('pointerdown', this.GroupCards, this);
      cPlayerCards.addChild(this.btnMeld);
      cPlayerCards.addChild(btnGroupspr);
      cPlayerCards.addChild(this.btnDiscard);
      cPlayerCards.addChild(btnSortspr);
      this.loadGameContainer.addChild(cPlayerCards);
    }

    public ArrangeCardsOnDrag(oCard)
    {
        console.log("ArrangeCardsOnDrag oCard = " + oCard);
        console.log("ArrangeCardsOnDrag this.bSortedCards = " + this.bSortedCards)
        console.log("ArrangeCardsOnDrag this.bGroupCards = " + this.bGroupCards)
        if(!this.bSortedCards && !this.bGroupCards)
        {
            console.log("ArrangeCardsOnDrag oCard.x = " + oCard.x)
            var bfound:boolean = false;
            for(var i:number = 0; i < this.arrPlayerCardSprite.length; ++i)
            {
                console.log("ArrangeCardsOnDrag this.arrPlayerCardSprite["+i+"].x = " + this.arrPlayerCardSprite[i].x)
                if(this.arrPlayerCardSprite[i].x > oCard.x && this.arrPlayerCardSprite[i] != oCard && !bfound)
                {
                   this.arrPlayerCardSprite = this.Addcard(i, oCard);
                   this.ArrangeCards();
                   bfound = true;
                   break;
                }
            }
            if(!bfound)   
            {
              this.arrPlayerCardSprite = this.Addcard(this.arrPlayerCardSprite.length, oCard);
              this.ArrangeCards();
            }
        }
        else if(this.bSortedCards)
        {

        }

    }

    private Addcard(nIndex, oCard)
    {
      var arrCards = new Array();
      for(var i:number = 0; i < this.arrPlayerCardSprite.length; ++i)
      {
        if(i == nIndex)
        {
          arrCards.push(oCard);
          arrCards.push(this.arrPlayerCardSprite[i]);
        }
        else if(this.arrPlayerCardSprite[i] != oCard)
        {
          arrCards.push(this.arrPlayerCardSprite[i]);
        }        
      }
      if(nIndex == this.arrPlayerCardSprite.length)
      {
        arrCards.push(oCard);
      }
      return arrCards;
    }

    private ArrangeCards()
    {
      for(var i:number = 0; i < this.arrPlayerCardSprite.length; ++i)
      {
        var oParent = this.arrPlayerCardSprite[i].parent;
        oParent.removeChild(this.arrPlayerCardSprite[i]);
        oParent.addChild(this.arrPlayerCardSprite[i]);
        this.arrPlayerCardSprite[i].x = 400 + (i*30);
      }
    }
  
    public OpenMeldPopUp(evtparams = null)
    {
      if(evtparams != null)
      {
        this.oMeldOpenCard = this.oDiscardCard;
      }
      if(this.bMelded)
      {
        return;
      }
      var Meld = new Container();
      this.arrMeldGroupCards = new Array();
      Meld.name ="MeldPopUp"
      const HeaderH = new Graphics();
      HeaderH.lineStyle(1, 0xFFFFFF, 1);
      HeaderH.beginFill(0x1d0033, 0.8);
      HeaderH.drawRoundedRect(190, 250, 900, 200, 25);
      HeaderH.endFill();
      Meld.addChild(HeaderH);
  
      const oGroup1 = new Graphics();
      oGroup1.name = "G1"
      oGroup1.lineStyle(1, 0xFFFFFF, 1);
      oGroup1.beginFill(0x3a0066, 0.8);
      oGroup1.drawRoundedRect(210, 265, 200, 130, 5);
      oGroup1.endFill();
      oGroup1.interactive = true;        
      oGroup1.buttonMode = true;
      oGroup1.on('pointerdown', this.InsertCardInSlots, this);
      Meld.addChild(oGroup1);
  
      const oGroup2 = new Graphics();
      oGroup2.name = "G2";
      oGroup2.lineStyle(1, 0xFFFFFF, 1);
      oGroup2.beginFill(0x3a0066, 0.9);
      oGroup2.drawRoundedRect(430, 265, 200, 130, 5);
      oGroup2.endFill();
      oGroup2.interactive = true;        
      oGroup2.buttonMode = true;
      oGroup2.on('pointerdown', this.InsertCardInSlots, this);
      Meld.addChild(oGroup2);    
  
      const oGroup3 = new Graphics();
      oGroup3.name = "G3";
      oGroup3.lineStyle(1, 0xFFFFFF, 1);
      oGroup3.beginFill(0x3a0066, 0.9);
      oGroup3.drawRoundedRect(650, 265, 200, 130, 5);
      oGroup3.endFill();
      oGroup3.interactive = true;        
      oGroup3.buttonMode = true;
      oGroup3.on('pointerdown', this.InsertCardInSlots, this);
      Meld.addChild(oGroup3);    
  
      const oGroup4 = new Graphics();
      oGroup4.name = "G4";
      oGroup4.lineStyle(1, 0xFFFFFF, 1);
      oGroup4.beginFill(0x3a0066, 0.9);
      oGroup4.drawRoundedRect(870, 265, 200, 130, 5);
      oGroup4.endFill();
      oGroup4.interactive = true;        
      oGroup4.buttonMode = true;
      oGroup4.on('pointerdown', this.InsertCardInSlots, this);
      Meld.addChild(oGroup4);
      const btnMeldcardspr = new Sprite(Texture.from(this.strBasePath + "finish.png"));
      btnMeldcardspr.name = "Meld";
      btnMeldcardspr.x = 950;
      btnMeldcardspr.y = 410;
      btnMeldcardspr.scale.x = 0.5;
      btnMeldcardspr.scale.y = 0.5;
      btnMeldcardspr.interactive = true;    
      btnMeldcardspr.buttonMode = true;
      btnMeldcardspr.on('pointerdown', this.MeldPlayerCards, this);
      Meld.addChild(btnMeldcardspr);
      this.loadGameContainer.addChild(Meld);
      this.oGroupCard = new Array();
    }
  
    private InsertCardInSlots(evtparams)
    {
      console.log("InsertCardInSlots evtparams.target.name = " + evtparams.target.name)
      console.log("InsertCardInSlots evtparams.currentTarget.name = " + evtparams.currentTarget.name)
      if(this.oGroupCard.length > 0)
      {
        var id:number;
        if(evtparams.target.name == "G1")
        {
          id = 1;
        }
        else if(evtparams.target.name == "G2")
        {
          id = 2;
        }
        else if(evtparams.target.name == "G3")
        {
          id = 3;
        }
        else if(evtparams.target.name == "G4")
        {
          id = 4;
        }
        var oParent = this.loadGameContainer.getChildByName("MeldPopUp");
        for(var i:number = 0; i < this.oGroupCard.length; ++i)
        {
          this.oGroupCard[i]["id"] = id; 
          var sprCard = this.mapPlayerCards.get(this.oGroupCard[i]);
          sprCard.scale.x = .8;
          sprCard.scale.y = .8;
          sprCard.y = 270;
          sprCard.x = this.arrMeldPos[id -1] + i * 30;
          sprCard.parent.removeChild(sprCard);
          oParent.addChild(sprCard)
          this.arrMeldGroupCards.push(this.oGroupCard[i]);
        }
        this.oGroupCard = new Array();
       
      }
    }
  
    private MeldPlayerCards(evtparams)
    {
      console.log("MeldPlayerCards evtparams = " + evtparams);
      console.log("MeldPlayerCards this.bCardPicked = " + this.bCardPicked)
      console.log("MeldPlayerCards this.oMeldOpenCard = " + this.oMeldOpenCard)
      var arrCards = new Array();
      this.DeactivateAfterDiscardbtns();
      if(this.bCardPicked && this.oMeldOpenCard != null)
      {       
        if(this.arrMeldGroupCards.length != 13)
        {
          for(var i:number = 0; i < this.oGroupsofCard.length; ++i)
          {
            for(var j:number = 0; j < this.oGroupsofCard[i].length; ++j)
            {
              arrCards.push(this.oGroupsofCard[i][j]);
            }      
          }
        }
        else{
            arrCards = this.arrMeldGroupCards;
        }
        global.app.getGameConfig().SendMeldCards(arrCards, this.oMeldOpenCard);
      }
      else{
        arrCards = this.arrMeldGroupCards;
        global.app.getGameConfig().SendMeldCards(arrCards, null);
      }
      this.bMelded = true;
      var oMeld = this.loadGameContainer.getChildByName("MeldPopUp");
      console.log("MeldPlayerCards oMeld = " + oMeld)
      oMeld.parent.removeChild(oMeld);
    }
  
    private GroupCards(evtparams)
    {
      console.log("GroupCards evtparams " + evtparams);
      if(this.oGroupsofCard == null)
      {
        this.oGroupsofCard = new Array();
      }
      if(this.oGroupsofCard.length == 0 && this.oGroupCard.length == 0)
      {
        return;
      }
      this.bGroupCards = true;
      console.log("this.oGroupsofCard.length = " + this.oGroupsofCard.length);
      for(var i:number = 0; i < this.oGroupsofCard.length; ++i)
      {  
        for(var j:number = 0; j < this.oGroupsofCard[i].length; ++j)
        {
          for(var k:number = 0; k < this.oGroupCard.length; ++k)
          {
            if(this.oGroupCard[k] == this.oGroupsofCard[i][j])
            {
              //this.oGroupsofCard[i] = this.oGroupsofCard[i].splice(j, 1)
              // console.log("oGroupCard["+ k +"] = " + this.oGroupCard[k] + " Card Matched");
              // console.log("oGroupCard["+ k +"].suit = " + this.oGroupCard[k].suit + " Card Matched");
              // console.log("oGroupCard["+ k +"].rank = " + this.oGroupCard[k].rank + " Card Matched");
              this.oGroupsofCard[i].splice(j, 1);
              --j;
            }
          }
        }      
      }
      for(var i:number = 0; i < this.oGroupsofCard.length; ++i)
      {
        // console.log("this.oGroupsofCard["+ i +"].length = " + this.oGroupsofCard[i].length);
        if(this.oGroupsofCard[i].length == 0)
        {
          this.oGroupsofCard.splice(i, 1);
          // console.log("after splice this.oGroupsofCard.length = " + this.oGroupsofCard.length);
          --i;
        }
      }
      this.oGroupsofCard.push(this.oGroupCard);
      this.oGroupCard = new Array();
      var xpost = 275;
      var pos:number;
      var arrTempCards = new Array();
      var index:number = 0;
      //console.log("this.oGroupsofCard.length = " + this.oGroupsofCard.length);    
      for(var i:number = 0; i < this.oGroupsofCard.length; ++i)
      {
        // console.log("xpost = " + xpost);
        console.log("i = " + i);
        //console.log("this.oGroupsofCard[i] = " + this.oGroupsofCard[i].length);
        for(var j:number = 0; j < this.oGroupsofCard[i].length; ++j)
        {
          var sprCard = this.mapPlayerCards.get(this.oGroupsofCard[i][j]);
          // console.log("j = " + j); 
          // console.log("xpost + (j * 30) = " + (xpost + (j * 30))); 
         // console.log("sprCard = " + sprCard); 
          if(sprCard != null && sprCard != undefined)   
          {
            sprCard.x = xpost + (j * 30);
            var cardParent = sprCard.parent;
            cardParent.removeChild(sprCard);
            cardParent.addChild(sprCard);
            pos =  sprCard.x + sprCard.width;
            for(var j = 0; j < sprCard.children.length;++j)
            {
              var oChild:Sprite = sprCard.getChildAt(j) as Sprite;
              oChild.tint = 0xFFFFFF;
            }
            //sprCard.tint = 0xFFFFFF;
            this.oGroupsofCard[i][j]["groupId"] = i+1;
            console.log("this.oGroupsofCard[i][j][groupId] = " + this.oGroupsofCard[i][j]["groupId"]); 
            arrTempCards.push(this.oGroupsofCard[i][j])
          }
          else{
            this.oGroupsofCard[i].splice(j, 1);
            --i;
          }
        }
        xpost = pos + 15;
        //console.log("xpost = " + xpost);
      }
      index = 0;
      // console.log("this.bSortedCards = " + this.bSortedCards);
      // console.log("xpost = " + xpost);
      // console.log("index = " + index);
      var bMatched:boolean = false;
      if(!this.bSortedCards)
      {     
        for(var i:number = 0; i < this.arrPlayerCards.length; ++i)
        {
          bMatched = false;
          for(var j:number = 0; j < arrTempCards.length; ++j)
          {
            if(arrTempCards[j] == this.arrPlayerCards[i])
            {
              bMatched = true;
              break;
            }
          }
          if(!bMatched)
          {
            var sprCard = this.mapPlayerCards.get(this.arrPlayerCards[i]); 
            if(sprCard != null && sprCard != undefined)   
            {         
              sprCard.x = xpost + (index * 30);
              var cardParent = sprCard.parent;
              cardParent.removeChild(sprCard);
              cardParent.addChild(sprCard);
              index++;
            }
            //xpost =  sprCard.x + 30;
          }
        }
      }else{
        // console.log("this.arrArrangedCards.length = " + this.arrArrangedCards.length);
        for(var i:number = 0; i < this.arrArrangedCards.length; ++i)
        {
          bMatched = false;
          // console.log("index = " + index);
          // console.log("this.arrArrangedCards[i] = " + this.arrArrangedCards[i]);
          for(var j:number = 0; j < arrTempCards.length; ++j)
          {
            if(arrTempCards[j] == this.arrArrangedCards[i])
            {
              bMatched = true;
              break;
            }
          }
          // console.log("bMatched = " + bMatched); 
          if(!bMatched)
          {
            var sprCard = this.mapPlayerCards.get(this.arrArrangedCards[i]); 
            // console.log("xpost + (index * 30) = " + (xpost + (index * 30)));
            // console.log("sprCard = " + sprCard); 
            if(sprCard != null && sprCard != undefined)   
            {
              sprCard.x = xpost + (index * 30);
              var cardParent = sprCard.parent;
              cardParent.removeChild(sprCard);
              cardParent.addChild(sprCard);
              ++index;
            }
          }
        }
      }
      console.log("GroupCards ####################################################");
    }
  
    private ResetCards()
    {
      for(var i = 0; i < this.oGroupCard.length; ++i)
      {
        var oCard = this.mapPlayerCards.get(this.oGroupCard[i]); 
        if(oCard != undefined && oCard != null)
        {
          for(var j = 0; j < oCard.children.length;++j)
          {
            var sprCard:Sprite = oCard.getChildAt(j) as Sprite;
            sprCard.tint = 0xFFFFFF;
          }
          //oCard.tint = 0xFFFFFF;
          //oCard.
        }
        this.oGroupCard.splice(i, 1);
        --i;
      }
      this.oGroupCard = new Array();
      this.oDiscardCard = null;
    }
  
    public OpenGameResult(arrChairs)
    {
      var arrHeaderText = ['Name', 'Result', 'Cards', 'Score', 'Total'];
      var oResult = new Container();
      oResult.name = "Result";
      var arrColor = [0x230b36, 0x0c7a3d]
      // var bg = new Sprite(Texture.from("/assets/img/Result.png"));
      // bg.name = "OpenDeck";
      // bg.scale.x = 0.67;
      // bg.scale.y = 0.67;
      // bg.x = 0;
      // bg.y = 0;
      // oResult.addChild(bg);
      const oResultbg = new Graphics();
      oResultbg.name = "G3";
      oResultbg.beginFill(0x230b36, 0.9);
      oResultbg.drawRoundedRect(37, 25, 1235, 665, 15);
      oResultbg.endFill();
      oResult.addChild(oResultbg);
      const oResultbg1 = new Graphics();
      oResultbg.name = "G3";
      oResultbg1.beginFill(0x574694, 0.9);
      oResultbg1.drawRect(37, 87, 1235, 545);
      oResultbg1.endFill();
      oResult.addChild(oResultbg1);
      
      const style = new TextStyle({fill:'#ffffff',fontWeight: 'bold',fontSize: 24});
      var arrXpos:number[] = [90, 235, 545,910, 1070];
      for(var i:number = 0; i < arrHeaderText.length; ++i)
      {
        const text = new Text(arrHeaderText[i], style);
        text.x = arrXpos[i];
        text.y = 40;
        oResult .addChild(text);
      }
      var nXpos:number = 35;
      var arrYpos:number[] = [114, 212, 545,910, 1070];
      for(var i:number = 0; i < arrChairs.length; ++i)
      {
        if(arrChairs[i].player != null || arrChairs[i].player != undefined)
        {
          if(arrChairs[i].player.playerStatus == "WINNER")
          {
            var oplayerInfo = this.StripePlayerDetailsDashboard(arrColor[1], arrChairs[i].player.userDetails.username, arrChairs[i].player.points, arrChairs[i].player.totalPointsForPoolGame, arrChairs[i].player.inHandCardList, true);
            oplayerInfo.x = nXpos;
            oplayerInfo.y = arrYpos[i];
            oResult.addChild(oplayerInfo);
          }
          else{
            var oplayerInfo = this.StripePlayerDetailsDashboard(arrColor[0], arrChairs[i].player.userDetails.username, arrChairs[i].player.points, arrChairs[i].player.totalPointsForPoolGame, arrChairs[i].player.inHandCardList);
            oplayerInfo.x = nXpos;
            oplayerInfo.y = arrYpos[i];
            oResult.addChild(oplayerInfo);
          }
        }
      }
      const txtNextGame = new Text("Get Ready - Next game will start shortly", style);
      txtNextGame.x = 400;
      txtNextGame.y = 650;
      oResult .addChild(txtNextGame);
      var strClose = new Sprite(Texture.from(this.strBasePath + "close_btn.png"));
      strClose.name = "close";
      strClose.scale.x = 0.67;
      strClose.scale.y = 0.67;
      strClose.x = 1238;
      strClose.y = 10;
      strClose.interactive = true;        
      strClose.buttonMode = true;
      strClose.on('pointerdown', this.CloseDashboard, this);
      oResult.addChild(strClose);
  
      var strPower = new Sprite(Texture.from(this.strBasePath + "power_button.png"));
      strPower.name = "close";
      strPower.scale.x = 0.11;
      strPower.scale.y = 0.11;
      strPower.x = 90;
      strPower.y = 637;
      strPower.interactive = true;        
      strPower.buttonMode = true;
      strPower.on('pointerdown', this.Power, this);
      oResult.addChild(strPower);
  
      var strPath = this.GetCardPath(this.oGameJoker);
      const sprJoker = new Sprite(Texture.from(strPath));
      sprJoker.name = this.oGameJoker.index.toString();
      sprJoker.scale.x = 0.3;
      sprJoker.scale.y = 0.3;
      sprJoker.x = 1130;    
      sprJoker.y = 637;    
      oResult.addChild(sprJoker);
  
      this.loadGameContainer.addChild(oResult);
  
      ;
      //this.oOpenDeckCard.on('pointerdown', this.TakeOpenCard, this);
    }
  
    private Power()
    {
      // var oDashboard = this.loadGameContainer.getChildByName("Result");
      // this.loadGameContainer.removeChild(oDashboard);
    }
  
    private CloseDashboard()
    {
      try{
        var oDashboard = this.loadGameContainer.getChildByName("Result");
        this.loadGameContainer.removeChild(oDashboard);
      }catch(e){}
      
    }
  
    private StripePlayerDetailsDashboard(color, username, points, totalPoints, arrCards = [], bwinner:boolean = false):Container
    {
      const style = new TextStyle({fill:'#ffffff',fontWeight: 'bold',fontSize: 24});
      var PlayerInfo = new Container();
      const oResultbgn = new Graphics();
      oResultbgn.name = username;
      oResultbgn.beginFill(color, 0.9);//0x0c7a3d
      oResultbgn.drawRect(0, 0, 1237, 67);
      oResultbgn.endFill();
      PlayerInfo.addChild(oResultbgn);
  
      const txtPlayerName = new Text(username, style);
      txtPlayerName.x = 60;
      txtPlayerName.y = 21;
      PlayerInfo.addChild(txtPlayerName);
  
      if(bwinner)
      {
        var winnerSpr = new Sprite(Texture.from(this.strBasePath + "trophy.png"));
        winnerSpr.name = "cup";
        winnerSpr.scale.x = 0.23;
        winnerSpr.scale.y = 0.23;
        winnerSpr.x = 220;
        winnerSpr.y = 16;
        PlayerInfo.addChild(winnerSpr);
      }
      else{
        const txtScore = new Text("LOST", style);
        txtScore.x = 210;
        txtScore.y = 21;
        PlayerInfo.addChild(txtScore);
      }
      var strPath;
      var nGroup:number; 
      var nXpos:number = 400;
      for(var i:number = 0; i < arrCards.length; ++i)
      {
        strPath = this.GetCardPath(arrCards[i]);
        const sprCard = new Sprite(Texture.from(strPath));
        sprCard.name = arrCards[i].index.toString();
        sprCard.scale.x = 0.3;
        sprCard.scale.y = 0.3;
        if(nGroup != arrCards[i]["groupId"])
        {
          nGroup = arrCards[i]["groupId"];
          nXpos +=30;
        }
        sprCard.x = nXpos + (i*10);
        
        sprCard.y = 5;
        PlayerInfo.addChild(sprCard);
      }
  
      const txtScore = new Text(points, style);
      txtScore.x = 900;
      txtScore.y = 21;
      PlayerInfo.addChild(txtScore);
  
      const txtTotalScrore = new Text(totalPoints, style);
      txtTotalScrore.x = 1050;
      txtTotalScrore.y = 21;
      PlayerInfo.addChild(txtTotalScrore);
      return PlayerInfo;
    }
  
    private DiscardPlayerCards(evtparams)
    {
      console.log("DiscardPlayerCards evtparams = " + evtparams);
      console.log("DiscardPlayerCards this.bCardPicked = " + this.bCardPicked);
      console.log("DiscardPlayerCards this.oDiscardCard = " + this.oDiscardCard);
      if(!this.bCardPicked || this.oDiscardCard == null)
      {
        return;
      }
      this.DeactivateAfterDiscardbtns();
    
      global.app.getGameConfig().SendDiscardEvent(this.oDiscardCard, false);
      var cPlayerCards = this.loadGameContainer.getChildByName("PlayerCards");
      cPlayerCards.removeChild(this.mapPlayerCards.get(this.oDiscardCard));
      var oCard = this.mapPlayerCards.get(this.oDiscardCard);
      
      for(var i = 0; i < this.arrPlayerCardSprite.length; ++i)
      {
        if(oCard == this.arrPlayerCardSprite[i])
        {
          this.arrPlayerCards.splice(i, 1);
          this.arrPlayerCardSprite.splice(i, 1);
          break;
        }
      }
      for(var i = 0; i < this.arrArrangedCards.length; ++i)
      {
        if(this.oDiscardCard == this.arrArrangedCards[i])
        {
          this.arrArrangedCards.splice(i, 1);
          break;
        }
      }
      for(var i = 0; i < this.oGroupsofCard.length; ++i)
      {
        for(var j = 0; j < this.oGroupsofCard[i].length; ++j)
       {
          if(this.oDiscardCard == this.oGroupsofCard[i][j])
          {
            this.oGroupsofCard[i].splice(j, 1);
            break;
          }
        }
      }
      this.mapPlayerCards.delete(this.oDiscardCard);
      this.bCardPicked = false;
      this.bCardDiscarded = true;
      this.oDiscardCard = null;
      global.app.getGameConfig().SendUpdateCards(this.arrPlayerCards);
    }
  
    private SortPlayerCards()
    {
      this.arrArrangedCards = new Array();
      this.oGroupsofCard = new Array();
      let arrSortCards:Map<string, Object> = new Map<string, Object>();
      this.bGroupCards = false;
      for(var i:number = 0; i < this.arrPlayerCards.length; ++i)
      {
        var arrCards;
        if(arrSortCards.get(this.arrPlayerCards[i].suit) != null)
        {
          arrCards = arrSortCards.get(this.arrPlayerCards[i].suit); 
        }
        else{
          arrCards = new Array();
        }
        arrCards.push(this.arrPlayerCards[i]);
        arrSortCards.set(this.arrPlayerCards[i].suit, arrCards)
          
      }
      var xpost = (window.innerWidth/6);
      for (let key of arrSortCards.keys()) {
  
        if(key != "JOKER")
        {
          var arrSorting:any = [];
          arrSorting = arrSortCards.get(key);
          arrSorting = arrSorting.sort(function(obj1, obj2) {return obj1.rank - obj2.rank;})
          var pos:number;
          for(var i:number = 0; i < arrSorting.length; ++i)
          {
            var sprCard = this.mapPlayerCards.get(arrSorting[i]);          
            sprCard.x = xpost + (i * 30);
            var cardParent = sprCard.parent;
            cardParent.removeChild(sprCard);
            cardParent.addChild(sprCard);
            pos =  sprCard.x + sprCard.width;
            this.arrArrangedCards.push(arrSorting[i]);
          }
          xpost = pos + 15;
        }
        this.bSortedCards = true;
      }
      if(arrSortCards.get("JOKER") != null)
      {
        var arrSorting:any = [];
        arrSorting = arrSortCards.get("JOKER");
        for(var i:number = 0; i < arrSorting.length; ++i)
        {
          var sprCard = this.mapPlayerCards.get(arrSorting[i]);          
          sprCard.x = xpost + (i * 30);
          var cardParent = sprCard.parent;
          cardParent.removeChild(sprCard);
          cardParent.addChild(sprCard);
          pos =  sprCard.x + sprCard.width;
          this.arrArrangedCards.push(arrSorting[i]);
        }
        xpost = pos + 15;
      }
      this.nNextXPos = xpost + 30;
    }
  
    public DisplayGameId(gameId, roundId)
    {
      const style = new TextStyle({ fill: '#ffffff', fontWeight: 'bold', fontSize: 16 });
  
      var txtGameId = new Text(gameId.toString(), style);
      txtGameId.x = 100;
      txtGameId.y = 20;
  
      var txtRoundId = new Text(roundId.toString(), style);
      txtRoundId.x = 370;
      txtRoundId.y = 20;
      this.loadGameContainer.addChild(txtRoundId);
      this.loadGameContainer.addChild(txtGameId);
    }
  
    public DisplayPracticeChips(totalChips)
    {
      const style = new TextStyle({ fill: '#ffffff', fontWeight: 'bold', fontSize: 16 });
  
      var txtTotalChips = new Text(totalChips.toString() + "Chips", style);
      txtTotalChips.x = 490;
      txtTotalChips.y = 20;    
      this.loadGameContainer.addChild(txtTotalChips);
    }
  
    public PlayerGameTimer(nTimer)
    {
      //this.bCardPicked = false;
      this.ResetCards();
      this.ClearGameTimer();
      var nCounter = nTimer;
      var oPlayerTimer;
      var txtTimer;
      try{
        if(this.loadGameContainer.getChildByName("OppPlayerTimer")!= null)
        {
          this.loadGameContainer.getChildByName("OppPlayerTimer").visible = false;
        }
        if(this.loadGameContainer.getChildByName("PlayerTimer")!= null)
        {
          oPlayerTimer = this.loadGameContainer.getChildByName("PlayerTimer");
          oPlayerTimer.visible = true;
          txtTimer = oPlayerTimer.getChildByName("timer");
          txtTimer.text = nCounter.toString();
          this.oGameTimer = setInterval(() => {
            nCounter--;
            txtTimer.text = nCounter.toString();
            
            if(nCounter <= 0)
            {
              clearInterval(this.oGameTimer);
            }
          }, 1000);
          return;
        }
        
      }catch(e){}
      const style = new TextStyle({ fill: '#ffffff', fontWeight: 'bold', fontSize: 16, align: 'center' });
      oPlayerTimer = new Container();
      oPlayerTimer.name = "PlayerTimer";
      const bgspr = new Sprite(Texture.from(this.strBasePath + "playerTurnTimer.png"));//game_play_non_player.jpg"));//background.png"));
      bgspr.name = "PlayerTimer";
      bgspr.scale.x = 0.5;
      bgspr.scale.y = 0.5;
      bgspr.x = 530;
      bgspr.y = 670;    
      oPlayerTimer.addChild(bgspr);
     
      txtTimer = new Text(nTimer.toString(), style);
      txtTimer.name = "timer";
      txtTimer.x = 548;
      txtTimer.y = 688;
  
     
      this.oGameTimer = setInterval(() => {
        nCounter--;
        txtTimer.text = nCounter.toString();
        
        if(nCounter <= 0)
        {
          clearInterval(this.oGameTimer);
        }
      }, 1000);
      oPlayerTimer.addChild(txtTimer);
      this.loadGameContainer.addChild(oPlayerTimer);
    }
  
    public ClearCards()
    {
      try{
      var cGameCards = this.loadGameContainer.getChildByName("GameCards");
      var oPlayerCards = this.loadGameContainer.getChildByName("PlayerCards");
      cGameCards.removeChildren();
      oPlayerCards.removeChildren();
      }catch(e){}
    }
  
    private ChangeOpenCard()
    {    
      var oOpenDeck = this.oGameCards.nextCard_In_OpenDeck_Top2;
      var cGameCards = this.loadGameContainer.getChildByName("GameCards");
      try
        {
          cGameCards.removeChild(this.oOpenDeckCard);
        }catch(e){}
      if(oOpenDeck != null)
      {     
        var strPath = this.GetCardPath(oOpenDeck);
        this.oOpenDeckCard = new Sprite(Texture.from(strPath));
        this.oOpenDeckCard.name = "OpenDeck";
        this.oOpenDeckCard.scale.x = 0.8;
        this.oOpenDeckCard.scale.y = 0.8;
        this.oOpenDeckCard.x = 520;
        this.oOpenDeckCard.y = 240;    
        this.oOpenCard = oOpenDeck;
        cGameCards.addChild(this.oOpenDeckCard);
      }
    }
  
    public TakeOpenCard(evtparams)
    {
      console.log("TakeOpenCard" + evtparams);    
      this.oPickedCard = this.oOpenCard;
      this.AddCardToPlayerDeck();
      this.ActivateAfterCardPickbtns();
      this.ChangeOpenCard();
    }

    private ActivateAfterCardPickbtns()
    {
      this.oCloseCardSlot.visible = true;
      this.btnMeld.visible = true;
      this.btnDiscard.visible = true;
    }

    private DeactivateAfterDiscardbtns()
    {
      this.oCloseCardSlot.visible = false;
      this.btnMeld.visible = false;
      this.btnDiscard.visible = false;
    }

    public TakeCloseCard(evtparams)
    {
      console.log("TakeCloseCard" + evtparams);    
      this.oPickedCard = this.oCloseCard;
      this.ActivateAfterCardPickbtns();
      this.AddCardToPlayerDeck();
    }
  
    private RemovePickedCard()
    {
      for(var i:number = 0; i < this.arrPlayerCards.length; ++i)
      {
        if(this.oPickedCard == this.arrPlayerCards[i])
        {
          var oCard = this.mapPlayerCards.get(this.arrPlayerCards[i]);
          this.arrPlayerCardSprite.splice(i, 1);
          this.arrPlayerCards.splice(i, 1);
          this.mapPlayerCards.delete(this.arrPlayerCards[i]);
          oCard.parent.removeChild(oCard);
          break;
        }
      }
      this.nNextXPos -= 15;
    }
  
    private AddCardToPlayerDeck()
    {
      this.bCardPicked = true;
      this.bCardDiscarded = false;
      var cPlayerCards = this.loadGameContainer.getChildByName("PlayerCards");
      this.oOpenDeckCard.interactive = false;        
      this.oOpenDeckCard.buttonMode = false;
      this.oClosedDeckCard.interactive = false;        
      this.oClosedDeckCard.buttonMode = false;
      var strPath = this.GetCardPath(this.oPickedCard);
      var oCard:Container = new Container();
      oCard.name = this.oPickedCard.index.toString() + "-" + this.oPickedCard.suit.toString()+ "-" + this.oPickedCard.rank.toString();
      const sprCard = new Sprite(Texture.from(strPath));//game_play_non_player.jpg"));//background.png"));
      sprCard.name = this.oPickedCard.index.toString();
      
      oCard.x = this.nNextXPos;
      oCard.scale.x = 0.7;
      oCard.scale.y = 0.7;
      oCard.y = (window.innerHeight/3) *2 -40;
      oCard.interactive = true;        
      oCard.buttonMode = true;
      this.nNextXPos = oCard.x + 15;
      oCard.on('pointerdown', this.CardClicked, this.oPickedCard);
      oCard.addChild(sprCard);
      if(this.oPickedCard.isCutJoker)
      {
        const sprJoker = new Sprite(Texture.from(this.strBasePath + "card_joker.png"));
        sprJoker.scale.x = 0.1;
        sprJoker.scale.y = 0.1;
        sprJoker.x = 5;
        sprJoker.y = 160;
        oCard.addChild(sprJoker);
      }
      cPlayerCards.addChild(oCard);
      //cPlayerCards.getChildByName("discard").visible = this.bCardPicked;
      this.arrPlayerCards.push(this.oPickedCard);
      this.arrPlayerCardSprite.push(oCard);
      this.mapPlayerCards.set(this.oPickedCard, oCard);
      global.app.getGameConfig().SendPickcardEvent(true);
    
    }
  
    public MeldCards(nTimer)
    {
      console.log("MeldCards nTimer = " + nTimer);  
    }
  
    public OppPlayerGameTimer(nTimer)
    {
      this.bCardPicked = false;
      var nCounter = nTimer;
      var oPlayerTimer;
      var txtTimer;
      this.ClearGameTimer();
      if(!this.bCardDiscarded)
      {
        this.RemovePickedCard();
      }
      try{
        if(this.loadGameContainer.getChildByName("PlayerTimer")!= null)
        {
          this.loadGameContainer.getChildByName("PlayerTimer").visible = false;
        }
        if(this.loadGameContainer.getChildByName("OppPlayerTimer")!= null)
        {
          oPlayerTimer = this.loadGameContainer.getChildByName("OppPlayerTimer");
          oPlayerTimer.visible = true;
          txtTimer = oPlayerTimer.getChildByName("timer");
          txtTimer.text = nCounter.toString();
          this.oGameTimer = setInterval(() => {
            nCounter--;
            txtTimer.text = nCounter.toString();
            
            if(nCounter <= 0)
            {
              clearInterval(this.oGameTimer);
            }
          }, 1000);
          return;
        }
        
      }catch(e){}
      const style = new TextStyle({ fill: '#ffffff', fontWeight: 'bold', fontSize: 16 , align: 'center'});
      oPlayerTimer = new Container();
      oPlayerTimer.name = "OppPlayerTimer";
      const bgspr = new Sprite(Texture.from(this.strBasePath + "playerTurnTimer.png"));//game_play_non_player.jpg"));//background.png"));
      bgspr.name = "PlayerTimer";
      bgspr.scale.x = 0.4;
      bgspr.scale.y = 0.4;
      bgspr.x = (window.innerWidth/2) - 100;
      bgspr.y = 80;    
      oPlayerTimer.addChild(bgspr);
     
      txtTimer = new Text(nTimer.toString(), style);
      txtTimer.name = "timer";
      txtTimer.x = (window.innerWidth/2) - 90;
      txtTimer.y = 95;
  
      //var nCounter = nTimer;
      this.oGameTimer = setInterval(() => {
        nCounter--;
        txtTimer.text = nCounter.toString();
        
        if(nCounter <= 0)
        {
          clearInterval(this.oGameTimer);
        }
      }, 1000);
      oPlayerTimer.addChild(txtTimer);
      this.oOpenDeckCard.interactive = false;        
      this.oOpenDeckCard.buttonMode = false;
      
      this.oClosedDeckCard.interactive = false;        
      this.oClosedDeckCard.buttonMode = false;
      this.loadGameContainer.addChild(oPlayerTimer);
    }
  
    private ClearGameTimer()
    {
      clearInterval(this.oGameTimer);
    }
  
    public ClearTimer()
    {
      clearInterval(this.oGameTimer); 
    }
}

