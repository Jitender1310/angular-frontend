import { Application, Container } from 'pixi.js';
//import { GameView } from '@views/GameView';
import { GameConfig } from '@app/GameConfig';
import '../style.css';

export class App {
  private app: Application;
  private container: Container;
  private gameConfig: GameConfig;
  private gameWindow;
  private access_token:string;
  private playnow_token:string;
  private strBasePath:string;
  private game_token:string;
  private bIsLobby:boolean = false;
  private bIsMobile:boolean = false;
  private game_type:string;

  constructor() {
    this.app = new Application({
      resizeTo: window,
      autoDensity: true,
      resolution: devicePixelRatio
    });
    document.body.appendChild(this.app.view);
    window.addEventListener('resize', () => {
      this.app.renderer.resize(window.innerWidth, window.innerHeight);
    });
    this.strBasePath = "/img/";
    // this.strBasePath = "/html5/www/img/";
    
    //console.log("window.location.href = " + window.location.href)
    var params = window.location.href.split("?")[1];
    if(params != undefined)
    {
      var arrparams = params.split("&");
      this.access_token = arrparams[0];
      this.playnow_token = arrparams[1];
      this.game_token = arrparams[2];
      this.game_type = arrparams[3];
    }
    
    if(this.playnow_token == null)
    {
      this.bIsLobby = true;
     this.bIsMobile = true;
    }
    console.log("bIsLobby = " + this.bIsLobby);
    console.log("this.bIsMobile  = " + this.bIsMobile);
    this.gameConfig = new GameConfig();
    
    this.container = new Container();
    this.app.stage.addChild(this.container);
  }

  public getIsLobby():boolean
  {
    return this.bIsLobby;
  }
  public getPlaynowToken(): string {
    return this.playnow_token;
  }
  public getGameToken(): string {
    return this.game_token;
  }
  public getGameType(): string {
    return this.game_type;
  }
  public getAccessToken(): string {
    return this.access_token;
  }
  public setAccessToken(access_token:string) {
    this.access_token = access_token;
  }

  public getContainer(): Container {
    return this.container;
  }

  public getGameConfig() {
    return this.gameConfig;
  }

  public getStage() {
    return this.app.stage;
  }

  public getBasePath() {
    return this.strBasePath;
  }

  public OpenWindow(url: string) {
    console.log("OpenWindow url  = " + url);
    console.log("OpenWindow this.bIsMobile  = " + this.bIsMobile);
    if (this.gameWindow == null && !this.bIsMobile) {
      this.gameWindow = window.open(url, '_blank', 'height=900,width=1200,menubar=1,resizable=no');
    }
    else {
      if(!this.bIsMobile)
      {
        if (!this.gameWindow.closed) {
          this.gameWindow.close();
        }
        this.gameWindow = window.open(url, '_blank', 'height=900,width=1200,menubar=1,resizable=no');
      }
      else{
        this.gameWindow = window.open(url, '_self', 'height=900,width=1200,menubar=1,resizable=no');
      }

    }
  }
}

export const app: App = new App();
