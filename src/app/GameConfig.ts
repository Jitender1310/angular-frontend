import { SmartFox, SFSEvent, SFSObject, LoginRequest, ExtensionRequest } from 'sfs2x-api';
import { Games } from 'entity/Games';
import * as global from "app";
import { Lobby } from '@views/Lobby';
import { GameBoard } from '@views/GameBoard';
import { Login } from '@views/Login';

export class GameConfig {
  //private repository:SFSObject;
  public sfs: SmartFox;
  private oDealListPractice: Array<Games> = new Array<Games>();
  private oDealListCash: Array<Games> = new Array<Games>();
  private oPointsListPractice: Array<Games> = new Array<Games>();
  private oPointsListCash: Array<Games> = new Array<Games>();
  private oPoolListPractice: Array<Games> = new Array<Games>();
  private oPoolListCash: Array<Games> = new Array<Games>();
  // private game_lobby: Login;
  private access_token: string;
  private user_name: string;
  private table_id:string;
  private user_id:string;
  private gender:string;
  public name:string;
  private game_token:string;
  private game_type:string;
  private playnow_token:string;
  private oLobby;
  private oLogin;
  private oGameBoard;
  public game_sub_type:string;

  constructor() {
    //this.sfs.connect("127.0.0.1", 9933);
    var config = { host: "rummydesk.com", port: 8080, useSSL: false, zone: "RummyZone", debug: false };
    this.sfs = new SmartFox(config);
    this.sfs.addEventListener(SFSEvent.CONNECTION, this.onConnection, this);
    this.sfs.addEventListener(SFSEvent.CONNECTION_LOST, this.onConnectionLost, this);
    this.sfs.addEventListener(SFSEvent.EXTENSION_RESPONSE, this.onExtensionResponse, this);
    this.sfs.addEventListener(SFSEvent.LOGIN, this.onLogin, this);
    this.sfs.addEventListener(SFSEvent.LOGIN_ERROR, this.onLoginError, this);
    //console.log("onRoomJoin global.app = " + global.app);
    
    this.sfs.connect();
  }

  public onRoomJoin(evtParams) {
    console.log("onRoomJoin evtParams = " + evtParams);
    var TempObj = new SFSObject();
    var date = new Date();
    TempObj.putUtfString("USER_GAME_TOKEN", date.getMilliseconds.toString());
    var Request = new ExtensionRequest("GAME_LIST", TempObj);
    this.sfs.send(Request);
  }

  public getGameBoard() {
    return this.oGameBoard;
  }

  public onLogin(evtParams) {
    console.log("onLogin zone = ", evtParams.zone);
    console.log("onLogin zone = ", evtParams.zone);
    if(!global.app.getIsLobby())
    {
      var login:SFSObject = (evtParams["data"]);
      var json:string = login.getText("RESPONSE_PACKET");
      var loginResponse:JSON = JSON.parse(json);
      
      if(loginResponse["success"])
      {
          if(global.app.getGameConfig().user_id == null)
          {
              global.app.getGameConfig().user_id = evtParams.user._id;
              var xhr = new XMLHttpRequest();
              xhr.open('POST', 'https://rummydesk.com/api/profile', true);
              xhr.onload = function() { global.app.getGameConfig().playerInfo(JSON.parse(this['responseText'])); };
              var formData = new FormData();
              formData.append("access_token", global.app.getGameConfig().access_token);
              xhr.send(formData);
          }
      }
    }
    else{
      this.getvals(this.LoadLobby);
    }
    
  }

  public playerInfo(evtParams)
  {
    var params = new SFSObject();
    this.gender = evtParams.data.gender;
    this.name = evtParams.data.username;
    params.putUtfString("GAME_ID", global.app.getGameConfig().game_token);
    params.putUtfString("USER_ID", global.app.getGameConfig().access_token);
    params.putUtfString("GAME_TYPE", global.app.getGameConfig().game_type);
    params.putUtfString("USER_GAME_TOKEN", global.app.getGameConfig().playnow_token);
    var Request = new ExtensionRequest("JOIN_TABLE", params);
    this.sfs.send(Request);
  }

  public getvals(cb) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'https://rummydesk.com/api/game_lobby', true);
    if (cb) xhr.onload = function () { cb(JSON.parse(this['responseText'])); console.log("getvals resp") };
    var formData = new FormData();
    formData.append("username", global.app.getGameConfig().user_name);
    formData.append("access_token", global.app.getGameConfig().access_token);
    xhr.send(formData);
  }

  public LoadLobby(evtParams) {
    console.log("LoadLobby evtParams = ", evtParams);//, JSON.stringify(evtParams));
    // console.log("LoadLobby root = ", document);
    global.app.getGameConfig().oDealListPractice = new Array<Games>();
    global.app.getGameConfig().oDealListCash = new Array<Games>();
    global.app.getGameConfig().oPointsListPractice = new Array<Games>();
    global.app.getGameConfig().oPointsListCash = new Array<Games>();
    global.app.getGameConfig().oPoolListPractice = new Array<Games>();
    global.app.getGameConfig().oPoolListCash = new Array<Games>();
    global.app.getGameConfig().oPoolListCash = evtParams.data.cash.pool;
    global.app.getGameConfig().oPoolListPractice = evtParams.data.practice.pool;
    global.app.getGameConfig().oDealListPractice = evtParams.data.practice.deals;
    global.app.getGameConfig().oDealListCash = evtParams.data.cash.deals;
    global.app.getGameConfig().oPointsListPractice = evtParams.data.practice.points;
    global.app.getGameConfig().oPointsListCash = evtParams.data.cash.points;

    // global.app.getGameConfig().game_lobby = new Login();
    //this.game_lobby = lobby;
    // global.app.getGameConfig().game_lobby.loadLobby();
    global.app.getGameConfig().oLobby = new Lobby();
    global.app.getGameConfig().oLobby.loadView();
  }

  public getLobby():Lobby
  {
    return this.oLobby;
  }

  public ClientPing() {
    console.log("ClientPing");
    var TempObj = new SFSObject();
    TempObj.putUtfString("USER_ID", global.app.getGameConfig().access_token);
    var Request = new ExtensionRequest("CLIENT_PING", TempObj);
    this.sfs.send(Request);
  }

  private startCountdown(seconds){
    //var counter = seconds;      
    console.log("seconds = " + seconds);
     var interval = setInterval(() => {
     this.ClientPing();
     console.log("interval = " + interval);
     }, 10000);
 }

 public SendPickcardEvent(bCloseDeck:boolean = false) {
  var params = new SFSObject();
  params.putBool("IS_CARD_PICKED_FROM_CLOSED_DECK", bCloseDeck);
  params.putUtfString("USER_ID", global.app.getGameConfig().access_token);
  params.putUtfString("TABLE_ID", global.app.getGameConfig().table_id);
  params.putUtfString("TABLE_EVENT_TYPE", "CARD_PICKED");
  params.putUtfString("GAME_TYPE", global.app.getGameConfig().game_type);
  params.putUtfString("USER_GAME_TOKEN", global.app.getGameConfig().playnow_token);
  var Request = new ExtensionRequest("GAME_TABLE_EVENT", params);
  this.sfs.send(Request);
}

public SendUpdateCards(arrCards:Array<Object>) {
  var params = new SFSObject();
  console.log("SendUpdateCards arrCards.length = " + arrCards.length);
  console.log("SendUpdateCards JSON.stringify(arrCards) = " + JSON.stringify(arrCards));
  var oJson = new Object();
  oJson["inHandCardList"] = arrCards;
  params.putUtfString("CARD_GROUPS", JSON.stringify(oJson));
  params.putUtfString("USER_ID", global.app.getGameConfig().access_token);
  params.putUtfString("TABLE_ID", global.app.getGameConfig().table_id);
  params.putUtfString("TABLE_EVENT_TYPE", "GROUP_IN_HAND_CARDS");
  params.putUtfString("GAME_TYPE", global.app.getGameConfig().game_type);
  params.putUtfString("USER_GAME_TOKEN", global.app.getGameConfig().playnow_token);
  var Request = new ExtensionRequest("GAME_TABLE_EVENT", params);
  console.log("SendPickcardEvent params = " + JSON.stringify(params));
  console.log("SendPickcardEvent params = " + JSON.stringify(Request));
  this.sfs.send(Request);
}

public SendMeldCards(arrCards:Array<Object>, oCard) {
  var params = new SFSObject();
  console.log("SendMeldCards arrCards.length = " + arrCards.length);
  console.log("SendMeldCards JSON.stringify(arrCards) = " + JSON.stringify(arrCards));
  var oJson = new Object();
  oJson["inHandCardList"] = arrCards;
  params.putBool("IS_DECLARE", true);
  params.putText("DISCARDED_CARD_BY_USER", JSON.stringify(oCard));
  params.putUtfString("CARD_GROUPS", JSON.stringify(oJson));
  params.putUtfString("USER_ID", global.app.getGameConfig().access_token);
  params.putUtfString("TABLE_ID", global.app.getGameConfig().table_id);
  params.putUtfString("TABLE_EVENT_TYPE", "REQUEST_MELD_CARD");
  params.putUtfString("GAME_TYPE", global.app.getGameConfig().game_type);
  params.putUtfString("USER_GAME_TOKEN", global.app.getGameConfig().playnow_token);
  var Request = new ExtensionRequest("GAME_TABLE_EVENT", params);
  console.log("SendMeldCards params = " + JSON.stringify(params));
  console.log("SendMeldCards params = " + JSON.stringify(Request));
  this.sfs.send(Request);
  //this.Result();
}


public SendDiscardEvent(oCard, bDeclare:boolean = false) {
  var params = new SFSObject();
  params.putBool("IS_DECLARE", bDeclare);
  params.putText("DISCARDED_CARD_BY_USER", JSON.stringify(oCard));
  params.putUtfString("USER_ID", global.app.getGameConfig().access_token);
  params.putUtfString("TABLE_ID", global.app.getGameConfig().table_id);
  params.putUtfString("TABLE_EVENT_TYPE", "FINISH_PLAYER_TURN");
  params.putUtfString("GAME_TYPE", global.app.getGameConfig().game_type);
  params.putUtfString("USER_GAME_TOKEN", global.app.getGameConfig().playnow_token);
  var Request = new ExtensionRequest("GAME_TABLE_EVENT", params);
  console.log("SendDiscardEvent params = " + JSON.stringify(params));
  console.log("SendDiscardEvent params = " + JSON.stringify(Request));
  this.sfs.send(Request);
}

public onExtensionResponse(evtParams) {
  // console.log("onExtensionResponse evtParams = " + Object.getOwnPropertyNames(evtParams));
   console.log("onExtensionResponse evtParams = " + JSON.stringify(evtParams));
  if(evtParams.cmd == "JOIN_TABLE")
  {
      var TempISFSObj:SFSObject = (evtParams["params"]);
      var json:string = TempISFSObj.getText("RESPONSE_PACKET");
      var Response:JSON = JSON.parse(json);
      var params = new SFSObject();
      params.putUtfString("TABLE_ID", Response["responsePacket"]);
      this.table_id = Response["responsePacket"];
      params.putUtfString("USER_ID", global.app.getGameConfig().access_token);
      params.putUtfString("TABLE_EVENT_TYPE", "TAKE_SEAT");
      params.putUtfString("GAME_TYPE", global.app.getGameConfig().game_type);
      params.putUtfString("USER_GAME_TOKEN", global.app.getGameConfig().playnow_token);//playnow_token
      var Request = new ExtensionRequest("GAME_TABLE_EVENT", params);
      this.sfs.send(Request);
      
      if(Response["success"])
      {
          global.app.getGameConfig().getGameBoard().loadView();
          
          // this.Result();
          //var arrCards = [{"index":36,"suit":"HEART","rank":11,"groupId":0,"isCutJoker":false},{"index":37,"suit":"HEART","rank":12,"groupId":0,"isCutJoker":false},{"index":91,"suit":"HEART","rank":13,"groupId":0,"isCutJoker":false},{"index":1,"suit":"CLUB","rank":2,"groupId":1,"isCutJoker":true},{"index":55,"suit":"CLUB","rank":3,"groupId":1,"isCutJoker":false},{"index":3,"suit":"CLUB","rank":4,"groupId":1,"isCutJoker":false},{"index":57,"suit":"CLUB","rank":5,"groupId":1,"isCutJoker":false},{"index":19,"suit":"DIAMOND","rank":7,"groupId":3,"isCutJoker":false},{"index":20,"suit":"DIAMOND","rank":8,"groupId":3,"isCutJoker":false},{"index":21,"suit":"DIAMOND","rank":9,"groupId":3,"isCutJoker":false},{"index":97,"suit":"SPADE","rank":6,"groupId":4,"isCutJoker":false},{"index":93,"suit":"SPADE","rank":2,"groupId":4,"isCutJoker":true},{"index":84,"suit":"HEART","rank":6,"groupId":4,"isCutJoker":false}];
          //this.getGameView().ViewPlayerCards(arrCards);
      }
  }
  if(evtParams.cmd == "GAME_TABLE_EVENT")
  {
      var TempISFSObj:SFSObject = (evtParams["params"]);
      var json:string = TempISFSObj.getText("RESPONSE_PACKET");
      console.log("onExtensionResponse TempISFSObj = " + TempISFSObj);
      console.log("onExtensionResponse json = " + json);
      var Response:JSON = JSON.parse(json);
      if(Response["success"])
      {
          if(Response["eventType"] == 'TAKE_SEAT')
          {
            // this.Result();
              console.log("onExtensionResponse TAKE_SEAT");
              this.game_sub_type = Response["responsePacket"]["gameDetail"]["gameTitle"] + " - " + Response["responsePacket"]["gameDetail"]["deals"] + " " + Response["responsePacket"]["gameDetail"]["gameSubType"];
              this.getGameBoard().AddPlayerImage(this.gender);
              var arrChairList = Response["responsePacket"]["tableChairList"]["chairs"];
              for(var i:number = 0; i < arrChairList.length; ++i)
              {
                  if(arrChairList[i]["player"] != undefined && arrChairList[i]["player"] != null)
                  {
                      if(arrChairList[i]["player"]["userDetails"]["username"] == this.name)
                      {
                          this.getGameBoard().DisplayPracticeChips(arrChairList[i]["player"]["userDetails"]["wallet_account"]["fun_chips"]);//wallet_account.fun_chips
                          break;
                      }                            
                  }
              }
              
              if(Response["responsePacket"]["tableStatus"] == "WaitingForPlayer" || Response["responsePacket"]["tableStatus"] == "None")
              {
                  this.getGameBoard().WaitingForPlayers();
                  this.getGameBoard().ClearTimer();
              }
              else{
                  this.getGameBoard().DisplayGameId(Response["responsePacket"].liveGame.gameId, Response["responsePacket"].roundCounter)
              }
              this.startCountdown(10);
          }
          if(Response["eventType"] == 'WAITING_FOR_GAME')
          {
              console.log("onExtensionResponse WAITING_FOR_GAME");
              var arrChairList = Response["responsePacket"]["tableChairList"]["chairs"];
              this.getGameBoard().AddUsersToTable(arrChairList);
              this.getGameBoard().WaitingForGame(Response["responsePacket"]["GAME_WAITING_MAX_TIME"]);
              //this.startCountdown(10);
          }
          if(Response["eventType"] == 'CUT_FOR_SEAT')
          {
              console.log("onExtensionResponse CUT_FOR_SEAT");
              var arrChairList = Response["responsePacket"]["tableChairList"]["chairs"];
              for(var i:number = 0; i < arrChairList.length; ++i)
              {
                  if(arrChairList[i]["player"] != undefined && arrChairList[i]["player"] != null)
                  {
                      if(arrChairList[i]["player"]["userDetails"]["username"] == this.name)
                      {
                          this.getGameBoard().ViewMyCutForSeatCard(arrChairList[i]["player"]["cutForSeatCard"]);
                      }
                      else{
                          this.getGameBoard().ViewOpponentCutForSeatCard(arrChairList[i]["player"]["cutForSeatCard"]);
                      }
                      
                  }
              }
          }
          if(Response["eventType"] == 'DEALING' || Response["eventType"] == 'EXISTING_PLAYER_REJOIN_TABLE')
          {
              var playerchairs = Response["responsePacket"].tableChairList.chairs;
              this.getGameBoard().ClearCards();
              for(var i:number = 0; i < playerchairs.length; ++i)
              {
                  if(playerchairs[i]["player"] != undefined && playerchairs[i]["player"] != null)
                  {
                      if(playerchairs[i]["player"]["userDetails"]["username"] == this.name)
                      {
                          this.getGameBoard().ViewPlayerCards(playerchairs[i].player.inHandCardList);
                          break;
                      }
                  }                        
              }
              this.getGameBoard().ViewGameCards(Response["responsePacket"].liveGame.gameCards);             
          }
          if(Response["eventType"] == 'EXISTING_PLAYER_REJOIN_TABLE')
          {
              var arrChairList = Response["responsePacket"]["tableChairList"]["chairs"];
              this.getGameBoard().AddUsersToTable(arrChairList);
              for(var i:number = 0; i < arrChairList.length; ++i)
              {
                  if(arrChairList[i]["player"] != undefined && arrChairList[i]["player"] != null)
                  {
                      if(arrChairList[i]["player"]["userDetails"]["username"] == this.name)
                      {
                          if(arrChairList[i]["player"]["turn"])
                          {
                              this.getGameBoard().PlayerGameTimer(Response["responsePacket"]["liveGame"]["PLAYER_TURN_MAX_TIME"]);
                              this.getGameBoard().ChangeGameCards(Response["responsePacket"].liveGame.gameCards, true);
                          }
                          else{
                              this.getGameBoard().OppPlayerGameTimer(Response["responsePacket"]["liveGame"]["PLAYER_TURN_MAX_TIME"]);
                              this.getGameBoard().ChangeGameCards(Response["responsePacket"].liveGame.gameCards);
                          }
                          break;
                      }
                  }                        
              }
          }
          if(Response["eventType"] == 'REQUEST_MELD_CARD')
          {
              this.getGameBoard().OpenMeldPopUp();
          }
          if(Response["eventType"] == 'MELD_CARD_RECEIVED')
          {
              this.getGameBoard().OpenGameResult(Response["responsePacket"].tableChairList.chairs);
          }
          if(Response["eventType"] == 'FINISH_PLAYER_TURN' ||  Response["eventType"] == 'TOGGLE_AUTO_PLAY')
          {
              var playerchairs = Response["responsePacket"].tableChairList.chairs;
              
              for(var i:number = 0; i < playerchairs.length; ++i)
              {
                  if(playerchairs[i]["player"] != undefined && playerchairs[i]["player"] != null)
                  {
                      if(playerchairs[i]["player"]["userDetails"]["username"] == this.name)
                      {
                          if(playerchairs[i]["player"]["turn"])
                          {
                              this.getGameBoard().PlayerGameTimer(Response["responsePacket"]["liveGame"]["PLAYER_TURN_MAX_TIME"]);
                              this.getGameBoard().ChangeGameCards(Response["responsePacket"].liveGame.gameCards, true);
                          }
                          else{
                              this.getGameBoard().OppPlayerGameTimer(Response["responsePacket"]["liveGame"]["PLAYER_TURN_MAX_TIME"]);
                              this.getGameBoard().ChangeGameCards(Response["responsePacket"].liveGame.gameCards);
                          }
                          break;
                      }
                  }                        
              }
          }
          if(Response["eventType"] == 'PLAYER_EXTRA_TIME')
          {
            var playerchairs = Response["responsePacket"].tableChairList.chairs;              
              for(var i:number = 0; i < playerchairs.length; ++i)
              {
                  if(playerchairs[i]["player"] != undefined && playerchairs[i]["player"] != null)
                  {
                      if(playerchairs[i]["player"]["userDetails"]["username"] == this.name)
                      {
                          if(playerchairs[i]["player"]["turn"])
                          {
                              this.getGameBoard().PlayerGameTimer(Response["responsePacket"]["liveGame"]["PLAYER_TURN_EXTRA_TIME"]);
                          }
                          else{
                              this.getGameBoard().OppPlayerGameTimer(Response["responsePacket"]["liveGame"]["PLAYER_TURN_EXTRA_TIME"]);
                          }
                          break;
                      }
                  }                        
              }
          }
      }
  }
}
// private Result()
// {
//   var arrCards = [{"index":36,"suit":"HEART","rank":11,"groupId":0,"isCutJoker":false},{"index":37,"suit":"HEART","rank":12,"groupId":0,"isCutJoker":false},{"index":91,"suit":"HEART","rank":13,"groupId":0,"isCutJoker":false},{"index":1,"suit":"CLUB","rank":2,"groupId":1,"isCutJoker":true},{"index":55,"suit":"CLUB","rank":3,"groupId":1,"isCutJoker":false},{"index":3,"suit":"CLUB","rank":4,"groupId":1,"isCutJoker":false},{"index":57,"suit":"CLUB","rank":5,"groupId":1,"isCutJoker":false},{"index":19,"suit":"DIAMOND","rank":7,"groupId":3,"isCutJoker":false},{"index":20,"suit":"DIAMOND","rank":8,"groupId":3,"isCutJoker":false},{"index":21,"suit":"DIAMOND","rank":9,"groupId":3,"isCutJoker":false},{"index":97,"suit":"SPADE","rank":6,"groupId":4,"isCutJoker":false},{"index":93,"suit":"SPADE","rank":2,"groupId":4,"isCutJoker":true},{"index":84,"suit":"HEART","rank":6,"groupId":4,"isCutJoker":false}];
//   this.getGameBoard().ViewPlayerCards(arrCards);
//   //this.getGameBoard().OpenMeldPopUp();
// }

  public getlobbyData(strGameType: String, strCurType: String) {
    if (strGameType == "deal") {
      if (strCurType == "cash") {
        return this.oDealListCash;
      }
      else {
        return this.oDealListPractice;
      }
    }
    else if (strGameType == "pool") {
      if (strCurType == "cash") {
        return this.oPoolListCash;
      }
      else {
        return this.oPoolListPractice;
      }
    }
    else {
      if (strCurType == "cash") {
        return this.oPointsListCash;
      }
      else {
        return this.oPointsListPractice;
      }
    }
  }

  // public getGameView() {
  //   return this.game_lobby;
  // }

  public onLoginError(evtParams) {
    console.log("onLoginError evtParams = ", evtParams);
  }

  public onConnection(evtParams) {
    console.log("onConnection evtParams = ", evtParams);
    if (evtParams.success) {
      if(!global.app.getIsLobby())
      {
        this.sfs.addEventListener(SFSEvent.ROOM_JOIN, this.onRoomJoin, this);
        this.game_token = global.app.getGameToken();
        this.access_token = global.app.getAccessToken();
        this.playnow_token = global.app.getPlaynowToken();
        this.game_type = global.app.getGameType();
        this.oGameBoard = new GameBoard();
      }
       this.WebLogin();
    }
    else {
      alert("Connection failed. Is the server running at all?");
      console.log("Connection failed. Is the server running at all?");
    }
  }

  public WebLogin() {
    if (global.app.getGameConfig().access_token == null) {
      // global.app.getGameConfig().game_table = new GameView();
      this.oLogin = new Login();
      this.oLogin.loadView();
    }
    else {
      //global.app.getGameConfig().user_id = evtParams.user._id;
      var xhr = new XMLHttpRequest();
      xhr.open('POST', 'https://rummydesk.com/api/profile', true);
      xhr.onload = function() { global.app.getGameConfig().WebLoginRes(JSON.parse(this['responseText'])); };
      var formData = new FormData();
      formData.append("access_token", global.app.getGameConfig().access_token);
      xhr.send(formData);//global.app.getGameConfig().WebLoginRes(null);

    }
    //xhr.send();
  }

  public openGame(cb) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://rummydesk.com/api/playnow_token?access_token=' + localStorage.getItem("access_token"), true);
    xhr.onload = function () { cb(JSON.parse(this['responseText'])); };
    var formData = new FormData();
    formData.append("access_token", localStorage.getItem("access_token"));
    xhr.send(formData);
  }

  public Login(userName: string, password: string, cb) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'https://rummydesk.com/api/login', true);
    if (cb) xhr.onload = function () { cb(JSON.parse(this['responseText'])); };
    var formData = new FormData();
    formData.append("username", userName);
    formData.append("password", password);
    xhr.send(formData);
  }

  public WebLoginRes(evtParams) {
    console.log("WebLoginRes evtParams = ", JSON.stringify(evtParams));
    global.app.getGameConfig().user_name = evtParams.data.username;
    global.app.getGameConfig().access_token = evtParams.data.access_token;
    global.app.setAccessToken(evtParams.data.access_token);
    var params = new SFSObject();
    params.putUtfString("REQUEST_TYPE", "LOGIN");
    params.putUtfString("USER_NAME", "");
    params.putUtfString("PASSWORD", "");
    params.putUtfString("USER_ID", global.app.getGameConfig().access_token);
    params.putUtfString("DEVICE_TOKEN", "");
    var req: LoginRequest;
    console.log("WebLoginRes params = ", params.get("USER_ID"));

    req = new LoginRequest(global.app.getGameConfig().user_name, "", params, "RummyZone");
    global.app.getGameConfig().sfs.send(req);

    //global.app.getGameConfig().getvals(global.app.getGameConfig().LoadLobby);

    //new SFS2X.LoginRequest("", "", null, "RummyZone"));
  }

  public onConnectionLost(evtParams) {
    console.log("onConnectionLost evtParams = ", evtParams);
    if (evtParams.success) {
      console.log("Connected to SmartFoxServer 2X!");
    }
    else {
      //global.app.getGameConfig().user_id = null;
      this.sfs.connect();
      console.log("Connection lost. Reconnecting.");
    }
  }

  public configure() {

  }
}