# Rummy Desk

A rummy game using Pixi.js and cordova.

## Install dependencies
``` npm install ```

## Build to cordova
``` npm run build ```

## Build Android or IOS

``` cd cordova ```

``` cordova platform add android ```

``` cordova build android ```
